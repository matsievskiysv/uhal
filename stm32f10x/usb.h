#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define USB_EP 0x4000##5c00##UL
#define USB    0x4000##5c40##UL

typedef union {
	struct __attribute__((packed)) {
		bool fres    : 1;
		bool pdwn    : 1;
		bool lp_mode : 1;
		bool fsusp   : 1;
		bool resume  : 1;
		bool	     : 1;
		bool	     : 1;
		bool	     : 1;
		bool esofm   : 1;
		bool sofm    : 1;
		bool resetm  : 1;
		bool suspm   : 1;
		bool wkupm   : 1;
		bool errm    : 1;
		bool pmaovrm : 1;
		bool ctrm    : 1;
	} bits;
	size_t val;
} usb_cntr_t;
_Static_assert(sizeof(usb_cntr_t) == sizeof(size_t), "invalid usb_cntr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint8_t ep_id : 4;
		bool	dir   : 1;
		uint8_t	      : 3;
		bool esof     : 1;
		bool sof      : 1;
		bool reset    : 1;
		bool susp     : 1;
		bool wkup     : 1;
		bool err      : 1;
		bool pmaovl   : 1;
		bool ctr      : 1;
	} bits;
	size_t val;
} usb_istr_t;
_Static_assert(sizeof(usb_istr_t) == sizeof(size_t), "invalid usb_istr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t fn   : 11;
		uint8_t	 lsof : 2;
		bool	 lck  : 1;
		bool	 rxdm : 1;
		bool	 rxdp : 1;
	} bits;
	size_t val;
} usb_fnr_t;
_Static_assert(sizeof(usb_fnr_t) == sizeof(size_t), "invalid usb_fnr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint8_t add : 7;
		bool	ef  : 1;
	} bits;
	size_t val;
} usb_daddr_t;
_Static_assert(sizeof(usb_daddr_t) == sizeof(size_t), "invalid usb_daddr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t btable : 16;
	} bits;
	size_t val;
} usb_btable_t;
_Static_assert(sizeof(usb_btable_t) == sizeof(size_t), "invalid usb_btable_t size");

typedef volatile struct {
	usb_cntr_t   cntr;
	usb_istr_t   istr;
	usb_fnr_t    fnr;
	usb_daddr_t  daddr;
	usb_btable_t btable;
} usb_t;
_Static_assert(sizeof(usb_t) == 5 * sizeof(size_t), "invalid usb_t size");

typedef union {
	struct __attribute__((packed)) {
		uint8_t ea	: 4;
		uint8_t stat_tx : 2;
		bool	dtog_tx : 1;
		bool	ctr_tx	: 1;
		bool	ep_kind : 1;
		uint8_t ep_type : 2;
		bool	setup	: 1;
		uint8_t stat_rx : 2;
		bool	dtog_rx : 1;
		bool	ctr_rx	: 1;
	} bits;
	size_t val;
} usb_epnr_t;
_Static_assert(sizeof(usb_epnr_t) == sizeof(size_t), "invalid usb_epnr_t size");

typedef enum {
	USB_ENDPOINT_STATUS_DISABLED = 0b00,
	USB_ENDPOINT_STATUS_STALL    = 0b01,
	USB_ENDPOINT_STATUS_NAK	     = 0b10,
	USB_ENDPOINT_STATUS_VALID    = 0b11,
} usb_endpoint_status_t;

typedef enum {
	USB_ENDPOINT_TYPE_BULK	    = 0b00,
	USB_ENDPOINT_TYPE_CONTROL   = 0b01,
	USB_ENDPOINT_TYPE_ISO	    = 0b10,
	USB_ENDPOINT_TYPE_INTERRUPT = 0b11,
} usb_endpoint_type_t;

typedef volatile struct {
	usb_epnr_t ep[8];
} usb_ep_t;
_Static_assert(sizeof(usb_ep_t) == 8 * sizeof(size_t), "invalid usb_ep_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t addrn_tx : 16;
	} bits;
	size_t val;
} usb_desc_addrn_tx_t;
_Static_assert(sizeof(usb_desc_addrn_tx_t) == sizeof(size_t), "invalid usb_desc_addrn_tx_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t countn_tx : 10;
	} bits;
	size_t val;
} usb_desc_countn_tx_t;
_Static_assert(sizeof(usb_desc_countn_tx_t) == sizeof(size_t), "invalid usb_desc_countn_tx_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t addrn_rx : 16;
	} bits;
	size_t val;
} usb_desc_addrn_rx_t;
_Static_assert(sizeof(usb_desc_addrn_rx_t) == sizeof(size_t), "invalid usb_desc_addrn_rx_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t countn_rx : 10;
		uint8_t	 num_block : 5;
		bool	 blsize	   : 1;
	} bits;
	size_t val;
} usb_desc_countn_rx_t;
_Static_assert(sizeof(usb_desc_countn_rx_t) == sizeof(size_t), "invalid usb_desc_countn_rx_t size");

typedef struct {
	usb_desc_addrn_tx_t  addr_tx;
	usb_desc_countn_tx_t count_tx;
	usb_desc_addrn_rx_t  addr_rx;
	usb_desc_countn_rx_t count_rx;
} usb_btable_entry_t;
_Static_assert(sizeof(usb_btable_entry_t) == 4 * sizeof(size_t), "invalid usb_btable_entry_t size");
