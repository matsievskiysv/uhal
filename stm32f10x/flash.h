#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define FLASH 0x4002##2000##UL

typedef union {
	struct __attribute__((packed)) {
		uint8_t latency : 3;
		bool	hlfcya	: 1;
		bool	prftbe	: 1;
		bool	prftbs	: 1;
	} bits;
	size_t val;
} flash_acr_t;
_Static_assert(sizeof(flash_acr_t) == sizeof(size_t), "invalid flash_acr_t size");

#define FLASH_LATENCY_0 0b000
#define FLASH_LATENCY_1 0b001
#define FLASH_LATENCY_2 0b010

typedef volatile struct {
	flash_acr_t acr;
} flash_t;
_Static_assert(sizeof(flash_t) == 1 * sizeof(size_t), "invalid flash_t size");
