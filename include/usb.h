#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/usb.h"
#elif defined(set32f103h)
#include "../stm32f103h/usb.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

#include "usb_descriptor.h"

#include <io.h>

/**
 * @brief Convert shared RAM global address to local address.
 */
#define SHARED_SRAM_GLOBAL_TO_LOCAL(REG) (((size_t) (REG) -SHARED_SRAM_BASE) / 2)

/**
 * @brief Convert shared RAM local address to global address.
 */
#define SHARED_SRAM_LOCAL_TO_GLOBAL(REG) (((size_t) (REG) *2) + SHARED_SRAM_BASE)

/**
 * @brief Mark variable to be located in shared RAM.
 */
#define SHARED_SRAM_REGION __attribute__((aligned(8), section("shared_ram")))

/**
 * @brief Convert shared RAM size from global to local.
 */
#define SIZE_GLOBAL_TO_LOCAL(SIZE) (SIZE / 2)

/**
 * @brief Convert shared RAM size from local to global.
 */
#define SIZE_LOCAL_TO_GLOBAL(SIZE) (SIZE * 2)

/**
 * @brief Get array size in shared RAM.
 */
#define SHARED_SRAM_ARRAY_SIZE(x, t) (SIZE_LOCAL_TO_GLOBAL(x) / sizeof(t))

/**
 * @brief Copy data from regular RAM to shared RAM.
 *
 * Regular RAM uses 32bit words; shared RAM uses 16bit words and when viewed from application contains 16bit empty
 * spaces.
 *
 * @param[out] dest Shared RAM destination buffer global address.
 * @param[in] src Regular RAM source address.
 * @param[in] n Byte count.
 */
void memcpy_ram2shared(void *__restrict__ dest, const void *__restrict__ src, size_t n);

/**
 * @brief Copy data from shared RAM to regular RAM.
 *
 * Regular RAM uses 32bit words; shared RAM uses 16bit words and when viewed from application contains 16bit empty
 * spaces.
 *
 * @param[out] dest Regular RAM destination address.
 * @param[in] src Shared RAM source buffer global address.
 * @param[in] n Byte count.
 */
void memcpy_shared2ram(void *__restrict__ dest, const void *__restrict__ src, size_t n);

/**
 * @brief USB btable instance.
 */
extern usb_btable_entry_t SHARED_SRAM_REGION usb_btable[];

/**
 * @brief USB endpoint zero receive buffer.
 *
 * USB standard requires full speed device endpoint zero to be of size @USB_EP0_SIZE.
 */
extern size_t SHARED_SRAM_REGION usb_buffer_ep0_rx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
_Static_assert(sizeof(usb_buffer_ep0_rx) == USB_EP0_SIZE * 2, "usb_buffer_ep0_rx must be of size USB_EP0_SIZE");

/**
 * @brief USB endpoint zero transmit buffer.
 *
 * USB standard requires full speed device endpoint zero to be of size @USB_EP0_SIZE.
 */
extern size_t SHARED_SRAM_REGION usb_buffer_ep0_tx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
_Static_assert(sizeof(usb_buffer_ep0_tx) == USB_EP0_SIZE * 2, "usb_buffer_ep0_tx must be of size USB_EP0_SIZE");

/**
 * @brief USB string descriptor index conversion helper.
 *
 * USB string descriptors are 1-based, stored in 0-based array.
 */
#define USB_STRING_IDX_TO_NUM(IDX) (IDX + 1)

/**
 * @brief USB string descriptor index conversion helper.
 *
 * USB string descriptors are 1-based, stored in 0-based array.
 */
#define USB_STRING_NUM_TO_IDX(NUM) (NUM - 1)

/**
 * @brief USB string descriptor list.
 *
 * Holds @strings array for @wLangID language ID.
 */
typedef struct {
	uint16_t wLangID;
	uint16_t string_count;
	wchar_t *strings[];
} usb_string_list_item_t;

/**
 * @brief USB string descriptor list for all supported languages.
 */
typedef struct {
	uint8_t			language_count;
	usb_string_list_item_t *langs[];
} usb_string_list_t;

/**
 * @brief USB string descriptor list for all supported languages instance.
 */
extern const usb_string_list_t usb_string_list;

/**
 * @brief Data chunk to be sent via USB.
 *
 * Buffer data may be sent in multiple transactions.
 */
typedef struct {
	void	*address; ///< Buffer address
	uint16_t size;	  ///< Buffer total size
	uint16_t remains; ///< Buffer byte count remains to be transferred.
} usb_sendbuffer_chunk_t;

/**
 * @brief Data chunks to be sent via USB.
 *
 * Designed to send static data arrays (i.e. USB descriptors) scattered across memory in multiple transactions.
 */
typedef struct {
	uint8_t		       chunk_count;
	usb_sendbuffer_chunk_t chunks[];
} usb_sendbuffer_chunks_t;

/**
 * @brief Send buffer, containing full device configuration to be send as per USB specification.
 *
 * USB specification requires all configuration related descriptors to be send in one go.
 */
extern usb_sendbuffer_chunks_t usb_full_configuraion_descriptor_buffer;

/**
 * @brief Push chunk to buffer.
 *
 * @param[in,out] chunks Buffer chunks state object.
 * @param[out] address Buffer chunk address.
 * @param[out] buffer_size Buffer chunk size.
 * @retval true Success.
 * @retval false No room in buffer.
 */
bool usb_sendbuffer_push_chunk(usb_sendbuffer_chunks_t *chunks, void *address, uint16_t buffer_size);

/**
 * @brief Prepare buffer to sending.
 *
 * @param[in,out] chunks Buffer chunks state object.
 */
void usb_endpoint_prepare_chunked(usb_sendbuffer_chunks_t *chunks);

/**
 * @brief Remove chunks from buffer.
 *
 * @param[in,out] chunks Buffer chunks state object.
 */
void usb_sendbuffer_flush_buffer(usb_sendbuffer_chunks_t *chunks);

/**
 * @brief Endpoint transmit type.
 */
typedef enum {
	USB_TRANSMIT_TYPE_NONE,	       ///< No data pending. Start receiving data.
	USB_TRANSMIT_TYPE_ADDRESS,     ///< Special case. Set device address and start receiving data.
	USB_TRANSMIT_TYPE_CHUNKED,     ///< Chunked array data pending.
	USB_TRANSMIT_TYPE_BUFFER,      ///< Buffer data pending.
	USB_TRANSMIT_TYPE_STRING,      ///< String data pending.
	USB_TRANSMIT_TYPE_UTF8_STRING, ///< USB string descriptor data pending.
} usb_transmit_type_t;

/**
 * @brief Endpoint state.
 */
typedef struct {
	uint8_t	 address       : 4;   ///< Endpoint address.
	uint8_t	 transmit_type : 3;   ///< Pending transmission type @usb_transmit_type_t.
	bool	 halt	       : 1;   ///< Endpoint halt flag.
	uint8_t	 endpoint_type : 2;   ///< Endpoint type @usb_transfer_type_t.
	uint16_t tx_expected_size;    ///< Expected USB transmission size.
	uint16_t tx_buffer_size;      ///< Transmission buffer size.
	uint16_t rx_buffer_size;      ///< Reception buffer size.
	void	*rx_buffer;	      ///< Transmission buffer address.
	void	*tx_buffer;	      ///< Reception buffer address.
	void (*rx_callback)(uint8_t); ///< Data reception callback.
	void (*tx_callback)(uint8_t); ///< Data transmission callback.
	void (*init_callback)(
	    uint8_t); ///< Endpoint initialization callback. Called when device transitions to CONFIGURED state.
	union {
		usb_sendbuffer_chunks_t *send_chunks; ///< Pending transmission chunk buffer pointer.
		uint8_t			*buffer;      ///< Pending transmission buffer pointer.
		char			*string;      ///< Pending transmission string pointer.
		wchar_t *wstring; ///< Pending transmission string pointer for string descriptor transfers.
		uint8_t	 function_address; ///< Function address to be set.
	};
} usb_endpoint_state_t;

/**
 * @brief USB device state.
 *
 * USB device state as per USB specification.
 */
typedef enum {
	USB_STATE_DEFAULT,
	USB_STATE_ADDRESS,
	USB_STATE_CONFIGURED,
} usb_device_state_t;

/**
 * @brief Global USB state.
 */
typedef struct {
	usb_device_state_t   state; ///< USB device state.
	uint8_t		     endpoint_count; ///< Number of USB endpoints.
	uint8_t		     configuration; ///< Selected configuration.
	uint8_t		     interface; ///< Selected interface.
	usb_endpoint_state_t endpoints[]; ///< Endpoints states.
} usb_global_state_t;

/**
 * @brief Global USB state instance.
 */
extern usb_global_state_t usb_global_state;

/**
 * @brief Start sending chunked buffer.
 *
 * @param[in] endp Endpoint index.
 * @param[in,out] chunks Chunk buffer.
 * @retval true Transmit buffer ready.
 * @retval false Nothing to transmit.
 */
bool usb_endpoint_transfer_chunked_begin(uint8_t endp, usb_sendbuffer_chunks_t *chunks);

/**
 * @brief Continue sending chunked buffer.
 *
 * @param[in] endp Endpoint index.
 * @retval true Transmit buffer ready.
 * @retval false Nothing to send.
 */
bool usb_endpoint_transfer_chunked_continue(uint8_t endp);

/**
 * @brief Start sending buffer in single transmission.
 *
 * Send buffer is truncated if does not fit in transmission buffer.
 *
 * @param[in] endp Endpoint index.
 * @param[in] buffer Send buffer.
 */
void usb_endpoint_transfer_single(uint8_t endp, const void *const buffer, uint16_t buffer_size);

/**
 * @brief Start zero sized transmission.
 *
 * Used in setup endpoint transactions.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_transfer_zero_size(uint8_t endp);

/**
 * @brief Send data from endpoint receive buffer.
 *
 * @param[in] endp Endpoint index.
 * @param[in] count Data size.
 */
void usb_endpoint_transfer_rx_buffer(uint8_t endp, uint16_t count);

/**
 * @brief Start sending string descriptor.
 *
 * @param[in] endp Endpoint index.
 * @param[in] string UTF8 encoding string to send.
 */
bool usb_endpoint_transfer_string_descriptor_begin(uint8_t endp, wchar_t *string);

/**
 * @brief Continue sending string descriptor.
 *
 * @param[in] endp Endpoint index.
 */
bool usb_endpoint_transfer_string_utf8_continue(uint8_t endp);

/**
 * @brief Start sending zero terminated string.
 *
 * @param[in] endp Endpoint index.
 * @param[in] string Zero terminated string to send.
 */
bool usb_endpoint_transfer_string_begin(uint8_t endp, char *string);

/**
 * @brief Continue sending zero terminated string.
 *
 * @param[in] endp Endpoint index.
 */
bool usb_endpoint_transfer_string_continue(uint8_t endp);

/**
 * @brief Start sending buffer.
 *
 * @param[in] endp Endpoint index.
 * @param[in] buffer Buffer to send.
 * @param[in] buffer_size Buffer size.
 */
bool usb_endpoint_transfer_buffer_begin(uint8_t endp, uint8_t *buffer, uint16_t buffer_size);

/**
 * @brief Continue sending buffer.
 *
 * @param[in] endp Endpoint index.
 */
bool usb_endpoint_transfer_buffer_continue(uint8_t endp);

/**
 * @brief Start receiving data.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_start_rx(uint8_t endp);

/**
 * @brief Start transmitting data.
 *
 * Data is transmitted on request from host.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_start_tx(uint8_t endp);

/**
 * @brief Return NACK on OUT requests.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_nack_rx(uint8_t endp);

/**
 * @brief Return NACK on IN requests.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_nack_tx(uint8_t endp);

/**
 * @brief Return STALL on OUT requests.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_stall_rx(uint8_t endp);

/**
 * @brief Return STALL on IN requests.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_stall_tx(uint8_t endp);

/**
 * @brief Ignore OUT requests.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_stop_rx(uint8_t endp);

/**
 * @brief Ignore IN requests.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_stop_tx(uint8_t endp);

/**
 * @brief Clear endpoint RX flag.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_clear_rx(uint8_t endp);

/**
 * @brief Clear endpoint TX flag.
 *
 * @param[in] endp Endpoint index.
 */
void usb_endpoint_clear_tx(uint8_t endp);

/**
 * @brief USB low priority interrupt handler.
 */
void usb_irqhandler();

/**
 * @brief USB endpoint transfer callback.
 *
 * Continues chunked transfers.
 */
void usb_handle_transfer(uint8_t ep);
