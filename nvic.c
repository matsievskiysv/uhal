#include <nvic.h>
#include <util.h>

void
enable_interrupt(size_t N)
{
	REG_T reg = GET_REG_OFFSET(NVIC_ISER, NVIC_REG_OFFSET(N));
	REG_SET(reg, NVIC_BIT(N));
}

void
disable_interrupt(size_t N)
{
	REG_T reg = GET_REG_OFFSET(NVIC_ICER, NVIC_REG_OFFSET(N));
	REG_SET(reg, NVIC_BIT(N));
}

bool
interrupt_pending(size_t N)
{
	REG_T reg = GET_REG_OFFSET(NVIC_ICPR, NVIC_REG_OFFSET(N));
	return REG_VAL(reg) & NVIC_BIT(N);
}

void
clear_interrupt(size_t N)
{
	REG_T reg = GET_REG_OFFSET(NVIC_ICPR, NVIC_REG_OFFSET(N));
	REG_SET(reg, NVIC_BIT(N));
}
