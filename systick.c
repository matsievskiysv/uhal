#include <systick.h>
#include <util.h>

void
systick_configure(size_t value, bool interrupt)
{
	systick_t    *systick  = GET_REG(SYSTICK);
	systick_csr_t csr_val  = {.bits = {.tickint = interrupt, .clksource = true}};
	systick_csr_t csr_mask = {.bits = {.tickint = true, .clksource = true}};
	REG_WRITE_MASK(&systick->csr.val, csr_val.val, csr_mask.val);
	systick_rvr_t rvr_val  = {.bits = {.reload = value}};
	systick_rvr_t rvr_mask = {.bits = {.reload = 0xffffff}};
	REG_WRITE_MASK(&systick->rvr.val, rvr_val.val, rvr_mask.val);
}

void
systick_start(void)
{
	systick_t    *systick = GET_REG(SYSTICK);
	systick_csr_t csr     = {.bits = {.enable = true}};
	REG_SET(&systick->csr.val, csr.val);
}

void
systick_stop(void)
{
	systick_t    *systick = GET_REG(SYSTICK);
	systick_csr_t csr     = {.bits = {.enable = true}};
	REG_CLEAR(&systick->csr.val, csr.val);
}

size_t
systick_current(void)
{
	systick_t *systick = GET_REG(SYSTICK);
	return systick->cvr.bits.current;
}

bool
systick_expired(void)
{
	systick_t *systick = GET_REG(SYSTICK);
	return systick->csr.bits.countflag;
}

void
systick_wait(size_t value)
{
	systick_configure(value, false);
	systick_start();
	WAIT_WHILE(!systick_expired());
}
