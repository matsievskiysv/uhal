#include <dma.h>
#include <util.h>

void
dma_config(size_t base, uint8_t channel, void *mem, void *phr, uint16_t size, bool mem2mem, uint8_t level,
	   uint8_t mem_sz, uint8_t phr_sz, bool mem_inc, bool phr_inc, bool circ, bool mem2phr, bool err_int,
	   bool half_int, bool full_int)
{
	dma_ch_t *dma_ch   = GET_REG_OFFSET(base, DMA_CH(channel));
	dma_ccr_t ccr_val  = {.bits = {
				  .en	   = false,
				  .mem2mem = mem2mem,
				  .pl	   = level,
				  .msize   = mem_sz,
				  .psize   = phr_sz,
				  .minc	   = mem_inc,
				  .pinc	   = phr_inc,
				  .circ	   = circ,
				  .dir	   = mem2phr,
				  .teie	   = err_int,
				  .htie	   = half_int,
				  .tcie	   = full_int,
			      }};
	dma_ccr_t ccr_mask = {.bits = {
				  .en	   = true,
				  .mem2mem = true,
				  .pl	   = DMA_PL_VHI,
				  .msize   = 0b11,
				  .psize   = 0b11,
				  .minc	   = true,
				  .pinc	   = true,
				  .circ	   = true,
				  .dir	   = true,
				  .teie	   = true,
				  .htie	   = true,
				  .tcie	   = true,
			      }};
	REG_WRITE_MASK(&dma_ch->ccr.val, ccr_val.val, ccr_mask.val);
	dma_cpar_t cpar = {.bits = {.pa = (size_t) phr}};
	REG_WRITE(&dma_ch->cpar.val, cpar.val);
	dma_cmar_t cmar = {.bits = {.ma = (size_t) mem}};
	REG_WRITE(&dma_ch->cmar.val, cmar.val);
	dma_cndtr_t cndtr = {.bits = {.ndt = size}};
	REG_WRITE(&dma_ch->cndtr.val, cndtr.val);
}

size_t
dma_get_mem_addr(size_t base, uint8_t channel)
{
	dma_ch_t *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	return dma_ch->cmar.val;
}

size_t
dma_get_phr_addr(size_t base, uint8_t channel)
{
	dma_ch_t *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	return dma_ch->cpar.val;
}

uint16_t
dma_get_count(size_t base, uint8_t channel)
{
	dma_ch_t *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	return dma_ch->cndtr.val;
}

void
dma_set_count(size_t base, uint8_t channel, uint16_t count)
{
	dma_ch_t   *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	dma_cndtr_t cndtr  = {.bits = {.ndt = count}};
	REG_WRITE(&dma_ch->cndtr.val, cndtr.val);
}

void
dma_set_mem_addr(size_t base, uint8_t channel, void *address)
{
	dma_ch_t  *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	dma_cmar_t cmar	  = {.bits = {.ma = (size_t) address}};
	REG_WRITE(&dma_ch->cmar.val, cmar.val);
}

void
dma_set_phr_addr(size_t base, uint8_t channel, void *address)
{
	dma_ch_t  *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	dma_cpar_t cpar	  = {.bits = {.pa = (size_t) address}};
	REG_WRITE(&dma_ch->cpar.val, cpar.val);
}

void
dma_enable(size_t base, uint8_t channel)
{
	dma_ch_t *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	dma_ccr_t ccr	 = {.bits = {.en = true}};
	REG_SET(&dma_ch->ccr.val, ccr.val);
}

void
dma_disable(size_t base, uint8_t channel)
{
	dma_ch_t *dma_ch = GET_REG_OFFSET(base, DMA_CH(channel));
	dma_ccr_t ccr	 = {.bits = {.en = true}};
	REG_CLEAR(&dma_ch->ccr.val, ccr.val);
}

dma_interrupt_t
dma_int_status(size_t base, uint8_t channel)
{
	dma_t	       *dma	= GET_REG(base);
	dma_interrupt_t dma_int = {.val = dma->isr.val};
	dma_int.val >>= (channel - 1) * 4;
	return dma_int;
}

void
dma_int_clear(size_t base, uint8_t channel, dma_interrupt_t mask)
{
	dma_t *dma = GET_REG(base);
	mask.val <<= (channel - 1) * 4;
	dma->ifcr.val = mask.val;
}
