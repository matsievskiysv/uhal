#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define ADC1 0x4001##2400##UL
#define ADC2 0x4001##2800##UL
#define ADC3 0x4001##3C00##UL

typedef union {
	struct __attribute__((packed)) {
		bool awd   : 1;
		bool eoc   : 1;
		bool jeoc  : 1;
		bool jstrt : 1;
		bool strt  : 1;
	} bits;
	size_t val;
} adc_sr_t;
_Static_assert(sizeof(adc_sr_t) == sizeof(size_t), "invalid adc_sr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint8_t awdch	: 5;
		bool	eocie	: 1;
		bool	awdie	: 1;
		bool	jeocie	: 1;
		bool	scan	: 1;
		bool	awdsgl	: 1;
		bool	jauto	: 1;
		bool	discen	: 1;
		bool	jdiscen : 1;
		uint8_t discnum : 3;
		uint8_t dualmod : 3;
		uint8_t		: 2;
		bool jawden	: 1;
		bool awden	: 1;
	} bits;
	size_t val;
} adc_cr1_t;
_Static_assert(sizeof(adc_cr1_t) == sizeof(size_t), "invalid adc_cr1_t size");

#define ADC_DUALMOD_IND		     0b0000
#define ADC_DUALMOD_COM_REG_INJ_SIM  0b0001
#define ADC_DUALMOD_COM_REG_ALT_TR   0b0010
#define ADC_DUALMOD_COM_INJ_FAST_INT 0b0011
#define ADC_DUALMOD_COM_INJ_SLOW_INT 0b0100
#define ADC_DUALMOD_INJ_SIM	     0b0101
#define ADC_DUALMOD_REG_SIM	     0b0110
#define ADC_DUALMOD_FAST_INT	     0b0111
#define ADC_DUALMOD_SLOW_INT	     0b1000
#define ADC_DUALMOD_ALT		     0b1001

#define ADC_DISCNUM_1 0b000
#define ADC_DISCNUM_2 0b001
#define ADC_DISCNUM_3 0b010
#define ADC_DISCNUM_4 0b011
#define ADC_DISCNUM_5 0b100
#define ADC_DISCNUM_6 0b101
#define ADC_DISCNUM_7 0b110
#define ADC_DISCNUM_8 0b111

#define ADC_AWDCH_0  0b00000
#define ADC_AWDCH_1  0b00001
#define ADC_AWDCH_2  0b00010
#define ADC_AWDCH_3  0b00011
#define ADC_AWDCH_4  0b00100
#define ADC_AWDCH_5  0b00101
#define ADC_AWDCH_6  0b00110
#define ADC_AWDCH_7  0b00111
#define ADC_AWDCH_8  0b01000
#define ADC_AWDCH_9  0b01001
#define ADC_AWDCH_10 0b01010
#define ADC_AWDCH_11 0b01011
#define ADC_AWDCH_12 0b01100
#define ADC_AWDCH_13 0b01101
#define ADC_AWDCH_14 0b01110
#define ADC_AWDCH_15 0b01111
#define ADC_AWDCH_16 0b10000
#define ADC_AWDCH_17 0b10001

typedef union {
	struct __attribute__((packed)) {
		bool adon	 : 1;
		bool cont	 : 1;
		bool cal	 : 1;
		bool rstcal	 : 1;
		uint8_t		 : 4;
		bool dma	 : 1;
		uint8_t		 : 2;
		bool	align	 : 1;
		uint8_t jextsel	 : 3;
		bool	jexttrig : 1;
		bool		 : 1;
		uint8_t extsel	 : 3;
		bool	extrtig	 : 1;
		bool	jswstart : 1;
		bool	swstart	 : 1;
		bool	tsvrefe	 : 1;
	} bits;
	size_t val;
} adc_cr2_t;
_Static_assert(sizeof(adc_cr2_t) == sizeof(size_t), "invalid adc_cr2_t size");

#define ADC1_EXTSEL_TIM_1_CC_1 0b000
#define ADC1_EXTSEL_TIM_1_CC_2 0b001
#define ADC1_EXTSEL_TIM_1_CC_3 0b010
#define ADC1_EXTSEL_TIM_2_CC_2 0b011
#define ADC1_EXTSEL_TIM_3_TRGO 0b100
#define ADC1_EXTSEL_TIM_4_CC_4 0b101
#define ADC1_EXTSEL_TIM_8_TRGO 0b110
#define ADC1_EXTSEL_EXTI_11    0b110
#define ADC1_EXTSEL_SWSTART    0b111

#define ADC2_EXTSEL_TIM_1_CC_1 ADC1_EXTSEL_TIM_1_CC_1
#define ADC2_EXTSEL_TIM_1_CC_2 ADC1_EXTSEL_TIM_1_CC_2
#define ADC2_EXTSEL_TIM_1_CC_3 ADC1_EXTSEL_TIM_1_CC_3
#define ADC2_EXTSEL_TIM_2_CC_2 ADC1_EXTSEL_TIM_2_CC_2
#define ADC2_EXTSEL_TIM_3_TRGO ADC1_EXTSEL_TIM_3_TRGO
#define ADC2_EXTSEL_TIM_4_CC_4 ADC1_EXTSEL_TIM_4_CC_4
#define ADC2_EXTSEL_TIM_8_TRGO ADC1_EXTSEL_TIM_8_TRGO
#define ADC2_EXTSEL_EXTI_11    ADC1_EXTSEL_EXTI_11
#define ADC2_EXTSEL_SWSTART    ADC1_EXTSEL_SWSTART

#define ADC3_EXTSEL_TIM_3_CC_1 0b000
#define ADC3_EXTSEL_TIM_2_CC_3 0b001
#define ADC3_EXTSEL_TIM_1_CC_3 0b010
#define ADC3_EXTSEL_TIM_8_CC_1 0b011
#define ADC3_EXTSEL_TIM_8_TRGO 0b100
#define ADC3_EXTSEL_TIM_5_CC_1 0b101
#define ADC3_EXTSEL_TIM_5_CC_3 0b110
#define ADC3_EXTSEL_SWSTART    0b111

#define ADC1_JEXTSEL_TIM_1_TRGO 0b000
#define ADC1_JEXTSEL_TIM_1_CC_4 0b001
#define ADC1_JEXTSEL_TIM_2_TRGO 0b010
#define ADC1_JEXTSEL_TIM_2_CC_1 0b011
#define ADC1_JEXTSEL_TIM_3_CC_4 0b100
#define ADC1_JEXTSEL_TIM_4_TRGO 0b101
#define ADC1_JEXTSEL_TIM_8_CC_4 0b110
#define ADC1_JEXTSEL_EXTI_15	0b110
#define ADC1_JEXTSEL_JWSTART	0b111

#define ADC2_JEXTSEL_TIM_1_TRGO ADC1_JEXTSEL_TIM_1_TRGO
#define ADC2_JEXTSEL_TIM_1_CC_4 ADC1_JEXTSEL_TIM_1_CC_4
#define ADC2_JEXTSEL_TIM_2_TRGO ADC1_JEXTSEL_TIM_2_TRGO
#define ADC2_JEXTSEL_TIM_2_CC_1 ADC1_JEXTSEL_TIM_2_CC_1
#define ADC2_JEXTSEL_TIM_3_CC_4 ADC1_JEXTSEL_TIM_3_CC_4
#define ADC2_JEXTSEL_TIM_4_TRGO ADC1_JEXTSEL_TIM_4_TRGO
#define ADC2_JEXTSEL_TIM_8_CC_4 ADC1_JEXTSEL_TIM_8_CC_4
#define ADC2_JEXTSEL_EXTI_15	ADC1_JEXTSEL_EXTI_15
#define ADC2_JEXTSEL_JWSTART	ADC1_JEXTSEL_JWSTART

#define ADC3_JEXTSEL_TIM_1_TRGO 0b000
#define ADC3_JEXTSEL_TIM_1_CC_4 0b001
#define ADC3_JEXTSEL_TIM_4_CC_3 0b010
#define ADC3_JEXTSEL_TIM_8_CC_2 0b011
#define ADC3_JEXTSEL_TIM_8_CC_4 0b100
#define ADC3_JEXTSEL_TIM_5_TRGO 0b101
#define ADC3_JEXTSEL_TIM_5_CC_4 0b110
#define ADC3_JEXTSEL_JWSTART	0b111

#define ADC_ALIGN_RIGHT 0
#define ADC_ALIGN_LEFT	1

typedef union {
	struct __attribute__((packed)) {
		size_t smp10 : 3;
		size_t smp11 : 3;
		size_t smp12 : 3;
		size_t smp13 : 3;
		size_t smp14 : 3;
		size_t smp15 : 3;
		size_t smp16 : 3;
		size_t smp17 : 3;
	} bits;
	size_t val;
} adc_smpr1_t;
_Static_assert(sizeof(adc_smpr1_t) == sizeof(size_t), "invalid adc_smpr1_t size");

#define ADC_SMP_1_5   0b000
#define ADC_SMP_7_5   0b001
#define ADC_SMP_13_5  0b010
#define ADC_SMP_28_5  0b011
#define ADC_SMP_41_5  0b100
#define ADC_SMP_55_5  0b101
#define ADC_SMP_71_5  0b110
#define ADC_SMP_239_5 0b111

typedef union {
	struct __attribute__((packed)) {
		size_t smp0 : 3;
		size_t smp1 : 3;
		size_t smp2 : 3;
		size_t smp3 : 3;
		size_t smp4 : 3;
		size_t smp5 : 3;
		size_t smp6 : 3;
		size_t smp7 : 3;
		size_t smp8 : 3;
		size_t smp9 : 3;
	} bits;
	size_t val;
} adc_smpr2_t;
_Static_assert(sizeof(adc_smpr2_t) == sizeof(size_t), "invalid adc_smpr2_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t joffset1 : 12;
	} bits;
	size_t val;
} adc_jofr1_t;
_Static_assert(sizeof(adc_jofr1_t) == sizeof(size_t), "invalid adc_jofr1_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t joffset2 : 12;
	} bits;
	size_t val;
} adc_jofr2_t;
_Static_assert(sizeof(adc_jofr2_t) == sizeof(size_t), "invalid adc_jofr2_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t joffset3 : 12;
	} bits;
	size_t val;
} adc_jofr3_t;
_Static_assert(sizeof(adc_jofr3_t) == sizeof(size_t), "invalid adc_jofr3_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t joffset4 : 12;
	} bits;
	size_t val;
} adc_jofr4_t;
_Static_assert(sizeof(adc_jofr4_t) == sizeof(size_t), "invalid adc_jofr4_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t ht : 12;
	} bits;
	size_t val;
} adc_htr_t;
_Static_assert(sizeof(adc_htr_t) == sizeof(size_t), "invalid adc_htr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t lt : 12;
	} bits;
	size_t val;
} adc_ltr_t;
_Static_assert(sizeof(adc_ltr_t) == sizeof(size_t), "invalid adc_ltr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t sq13 : 5;
		uint16_t sq14 : 5;
		uint16_t sq15 : 5;
		uint16_t sq16 : 5;
		uint8_t	 l    : 4;
	} bits;
	size_t val;
} adc_sqr1_t;
_Static_assert(sizeof(adc_sqr1_t) == sizeof(size_t), "invalid adc_sqr1_t size");

#define ADC_L_1	 0b0000
#define ADC_L_2	 0b0001
#define ADC_L_3	 0b0010
#define ADC_L_4	 0b0011
#define ADC_L_5	 0b0100
#define ADC_L_6	 0b0101
#define ADC_L_7	 0b0110
#define ADC_L_8	 0b0111
#define ADC_L_9	 0b1000
#define ADC_L_10 0b1001
#define ADC_L_11 0b1010
#define ADC_L_12 0b1011
#define ADC_L_13 0b1100
#define ADC_L_14 0b1101
#define ADC_L_15 0b1110
#define ADC_L_16 0b1111

typedef union {
	struct __attribute__((packed)) {
		uint16_t sq7  : 5;
		uint16_t sq16 : 5;
		uint16_t sq9  : 5;
		uint16_t sq10 : 5;
		uint16_t sq11 : 5;
		uint16_t sq12 : 5;
	} bits;
	size_t val;
} adc_sqr2_t;
_Static_assert(sizeof(adc_sqr2_t) == sizeof(size_t), "invalid adc_sqr2_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t sq1 : 5;
		uint16_t sq2 : 5;
		uint16_t sq3 : 5;
		uint16_t sq4 : 5;
		uint16_t sq5 : 5;
		uint16_t sq6 : 5;
	} bits;
	size_t val;
} adc_sqr3_t;
_Static_assert(sizeof(adc_sqr3_t) == sizeof(size_t), "invalid adc_sqr3_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t jsq1 : 5;
		uint16_t jsq2 : 5;
		uint16_t jsq3 : 5;
		uint16_t jsq4 : 5;
		uint8_t	 jl   : 2;
	} bits;
	size_t val;
} adc_jsqr_t;
_Static_assert(sizeof(adc_jsqr_t) == sizeof(size_t), "invalid adc_jsqr_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t jdata : 16;
	} bits;
	size_t val;
} adc_jdr1_t;
_Static_assert(sizeof(adc_jdr1_t) == sizeof(size_t), "invalid adc_jdr1_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t jdata : 16;
	} bits;
	size_t val;
} adc_jdr2_t;
_Static_assert(sizeof(adc_jdr2_t) == sizeof(size_t), "invalid adc_jdr2_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t jdata : 16;
	} bits;
	size_t val;
} adc_jdr3_t;
_Static_assert(sizeof(adc_jdr3_t) == sizeof(size_t), "invalid adc_jdr3_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t jdata : 16;
	} bits;
	size_t val;
} adc_jdr4_t;
_Static_assert(sizeof(adc_jdr4_t) == sizeof(size_t), "invalid adc_jdr4_t size");

typedef union {
	struct __attribute__((packed)) {
		uint16_t adc2data : 16;
		uint16_t data	  : 16;
	} bits;
	size_t val;
} adc_dr_t;
_Static_assert(sizeof(adc_dr_t) == sizeof(size_t), "invalid adc_dr_t size");

typedef volatile struct {
	adc_sr_t    sr;
	adc_cr1_t   cr1;
	adc_cr2_t   cr2;
	adc_smpr1_t smpr1;
	adc_smpr2_t smpr2;
	adc_jofr1_t jofr1;
	adc_jofr2_t jofr2;
	adc_jofr3_t jofr3;
	adc_jofr4_t jofr4;
	adc_htr_t   htr;
	adc_ltr_t   ltr;
	adc_sqr1_t  sqr1;
	adc_sqr2_t  sqr2;
	adc_sqr3_t  sqr3;
	adc_jsqr_t  jsqr;
	adc_jdr1_t  jdr1;
	adc_jdr2_t  jdr2;
	adc_jdr3_t  jdr3;
	adc_jdr4_t  jdr4;
	adc_dr_t    dr;
} adc_t;
_Static_assert(sizeof(adc_t) == 20 * sizeof(size_t), "invalid adc_t size");
