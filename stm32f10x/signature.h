#pragma once

#include <stddef.h>
#include <stdint.h>

#define FLASH_SZ  0x1FFF##F7E0##UL
#define UNIQUE_ID 0x1FFF##F7E8##UL

typedef union {
	struct __attribute__((packed)) {
		const uint16_t f_size;
	} bits;
	const size_t val;
} f_size_t;
_Static_assert(sizeof(f_size_t) == sizeof(size_t), "invalid f_size_t size");

typedef struct __attribute__((packed)) {
	const uint16_t id[6];
} unique_id_t;
_Static_assert(sizeof(unique_id_t) == 6 * sizeof(uint16_t), "invalid unique_id_t size");
