#include <string.h>
#include <usb.h>
#include <util.h>
#include <wchar.h>

static int8_t
usb_endpoint_address_to_index(uint8_t address)
{
	for (uint8_t idx = 0; idx < usb_global_state.endpoint_count; idx++)
		if (usb_global_state.endpoints[idx].address == address)
			return idx;
	return -1;
}

bool
usb_sendbuffer_push_chunk(usb_sendbuffer_chunks_t *chunks, void *address, uint16_t size)
{
	for (size_t i = 0; i < chunks->chunk_count; ++i) {
		if (chunks->chunks[i].size == 0) {
			chunks->chunks[i].address = address;
			chunks->chunks[i].size	  = size;
			chunks->chunks[i].remains = size;
			return true;
		}
	}
	return false;
}

void
usb_endpoint_prepare_chunked(usb_sendbuffer_chunks_t *chunks)
{
	for (size_t i = 0; i < chunks->chunk_count; ++i)
		if (chunks->chunks[i].size != 0)
			chunks->chunks[i].remains = chunks->chunks[i].size;
}

void
usb_sendbuffer_flush_buffer(usb_sendbuffer_chunks_t *chunks)
{
	for (size_t i = 0; i < chunks->chunk_count; ++i)
		chunks->chunks[i].size = 0;
}

static bool
endpoint_transfer_chunked(uint8_t endp, usb_sendbuffer_chunks_t *chunks)
{
	volatile usb_btable_entry_t *const endp_btable = &usb_btable[endp];
	if (usb_global_state.endpoints[endp].tx_expected_size == 0) {
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_NONE;
		return false;
	}
	uint16_t transmit_size =
	    MIN(usb_global_state.endpoints[endp].tx_buffer_size, usb_global_state.endpoints[endp].tx_expected_size);
	uint8_t	 buffer[transmit_size];
	uint8_t *buffer_pointer = buffer;
	uint16_t remains	= transmit_size;

	for (uint8_t i = 0; i < chunks->chunk_count; ++i) {
		if (remains == 0)
			break;

		if ((chunks->chunks[i].size > 0) && (chunks->chunks[i].remains > 0)) {
			uint16_t chunk_size = MIN(chunks->chunks[i].remains, remains);
			memcpy(buffer_pointer,
			       chunks->chunks[i].address + chunks->chunks[i].size - chunks->chunks[i].remains,
			       chunk_size);
			buffer_pointer += chunk_size;
			remains -= chunk_size;
			chunks->chunks[i].remains -= chunk_size;
		}
	}
	if (remains == transmit_size) {
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_NONE;
		return false;
	}

	usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_CHUNKED;
	transmit_size -= remains;
	memcpy_ram2shared((void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx), buffer,
			  transmit_size);
	REG_WRITE(&endp_btable->count_tx.val, transmit_size);
	usb_global_state.endpoints[endp].tx_expected_size -= transmit_size;
	return true;
}

bool
usb_endpoint_transfer_chunked_begin(uint8_t endp, usb_sendbuffer_chunks_t *chunks)
{
	return endpoint_transfer_chunked(endp, chunks);
}

bool
usb_endpoint_transfer_chunked_continue(uint8_t endp)
{
	return endpoint_transfer_chunked(endp, usb_global_state.endpoints[endp].send_chunks);
}

void
usb_endpoint_transfer_single(uint8_t endp, const void *const buffer, uint16_t buffer_size)
{
	volatile usb_btable_entry_t *const endp_btable = &usb_btable[endp];

	size_t transmit_size = MIN(usb_global_state.endpoints[endp].tx_buffer_size, buffer_size);
	memcpy_ram2shared((void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx), buffer,
			  transmit_size);
	REG_WRITE(&endp_btable->count_tx.val, transmit_size);
}

void
usb_endpoint_transfer_zero_size(uint8_t endp)
{
	volatile usb_btable_entry_t *const endp_btable = &usb_btable[endp];
	REG_WRITE(&endp_btable->count_tx.val, 0);
}

void
usb_endpoint_transfer_rx_buffer(uint8_t endp, uint16_t count)
{
	volatile usb_btable_entry_t *const endp_btable = &usb_btable[endp];
	uint16_t			   transfer_size =
	    MIN(MIN(usb_global_state.endpoints[endp].rx_buffer_size, usb_global_state.endpoints[endp].tx_buffer_size),
		count);
	void *buffer_rx = (void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_rx.bits.addrn_rx);
	void *buffer_tx = (void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx);
	memcpy(buffer_tx, buffer_rx, transfer_size);
	REG_WRITE(&endp_btable->count_tx.val, transfer_size);
}

bool
usb_endpoint_transfer_string_descriptor_begin(uint8_t endp, wchar_t *string)
{
	volatile usb_btable_entry_t *const endp_btable	 = &usb_btable[endp];
	uint8_t				   string_length = MIN(wcslen(string), ((254 - 2) / 2));
	if ((usb_global_state.endpoints[endp].tx_expected_size == 0) || !string_length)
		return false;
	uint8_t	 descriptor_size = string_length * sizeof(uint16_t) + 2;
	uint16_t transmit_size =
	    MIN(MIN(usb_global_state.endpoints[endp].tx_buffer_size, usb_global_state.endpoints[endp].tx_expected_size),
		descriptor_size);
	uint8_t buffer[transmit_size];
	buffer[0]		= descriptor_size;
	buffer[1]		= USB_DESCRIPTOR_STRING;
	uint16_t *string_buffer = (uint16_t *) (buffer + 2);

	for (size_t idx = 0; idx < (transmit_size - 2) / sizeof(uint16_t); idx++)
		string_buffer[idx] = *string++;

	if (wcslen(string)) {
		usb_global_state.endpoints[endp].wstring       = string;
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_UTF8_STRING;
	}
	memcpy_ram2shared((void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx), buffer,
			  transmit_size);
	REG_WRITE(&endp_btable->count_tx.val, transmit_size);
	usb_global_state.endpoints[endp].tx_expected_size -= transmit_size;
	return true;
}

bool
usb_endpoint_transfer_string_utf8_continue(uint8_t endp)
{
	volatile usb_btable_entry_t *const endp_btable = &usb_btable[endp];
	if ((!usb_global_state.endpoints[endp].wstring) || (usb_global_state.endpoints[endp].tx_expected_size == 0) ||
	    !wcslen(usb_global_state.endpoints[endp].wstring))
		return false;
	uint8_t	 string_length = MIN(wcslen(usb_global_state.endpoints[endp].wstring), ((254 - 2) / 2));
	uint16_t transmit_size =
	    MIN(MIN(usb_global_state.endpoints[endp].tx_buffer_size, usb_global_state.endpoints[endp].tx_expected_size),
		string_length * sizeof(uint16_t));
	uint8_t	  buffer[transmit_size];
	uint16_t *string_buffer = (uint16_t *) buffer;

	for (size_t idx = 0; idx < (transmit_size) / sizeof(uint16_t); idx++)
		string_buffer[idx] = *usb_global_state.endpoints[endp].wstring++;

	memcpy_ram2shared((void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx), buffer,
			  transmit_size);
	REG_WRITE(&endp_btable->count_tx.val, transmit_size);
	usb_global_state.endpoints[endp].tx_expected_size -= transmit_size;
	return true;
}

static bool
endpoint_transfer_string(uint8_t endp, char *string)
{
	volatile usb_btable_entry_t *const endp_btable	 = &usb_btable[endp];
	if (!string || !strlen(string)) {
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_NONE;
		return false;
	}
	uint16_t transmit_size = MIN(usb_global_state.endpoints[endp].tx_buffer_size, strlen(string));

	memcpy_ram2shared((void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx), string,
			  transmit_size);
	string += transmit_size;
	if (strlen(string)) {
		usb_global_state.endpoints[endp].string	       = string;
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_STRING;
	} else {
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_NONE;
	}
	REG_WRITE(&endp_btable->count_tx.val, transmit_size);
	return true;
}

bool
usb_endpoint_transfer_string_begin(uint8_t endp, char *string)
{
	return endpoint_transfer_string(endp, string);
}

bool
usb_endpoint_transfer_string_continue(uint8_t endp)
{
	return endpoint_transfer_string(endp, usb_global_state.endpoints[endp].string);
}

static bool
endpoint_transfer_buffer(uint8_t endp, uint8_t *buffer, uint16_t buffer_size)
{
	volatile usb_btable_entry_t *const endp_btable = &usb_btable[endp];
	if (usb_global_state.endpoints[endp].tx_expected_size == 0) {
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_NONE;
		return false;
	}
	uint16_t transmit_size = MIN(usb_global_state.endpoints[endp].tx_buffer_size, buffer_size);

	memcpy_ram2shared((void *) SHARED_SRAM_LOCAL_TO_GLOBAL(endp_btable->addr_tx.bits.addrn_tx), buffer,
			  transmit_size);
	buffer_size -= transmit_size;
	if (buffer_size) {
		usb_global_state.endpoints[endp].buffer	       = buffer + transmit_size;
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_BUFFER;
	} else {
		usb_global_state.endpoints[endp].transmit_type = USB_TRANSMIT_TYPE_NONE;
	}
	REG_WRITE(&endp_btable->count_tx.val, transmit_size);
	usb_global_state.endpoints[endp].tx_expected_size = buffer_size;
	return true;
}

bool
usb_endpoint_transfer_buffer_begin(uint8_t endp, uint8_t *buffer, uint16_t buffer_size)
{
	return endpoint_transfer_buffer(endp, buffer, buffer_size);
}

bool
usb_endpoint_transfer_buffer_continue(uint8_t endp)
{
	return endpoint_transfer_buffer(endp, usb_global_state.endpoints[endp].buffer,
					usb_global_state.endpoints[endp].tx_expected_size);
}

void
memcpy_ram2shared(void *__restrict__ dest, const void *__restrict__ src, size_t n)
{
	while (n > 0) {
		if (n >= sizeof(uint16_t)) {
			const uint16_t *src_words  = src;
			uint32_t       *dest_words = dest;
			dest_words[0]		   = src_words[0];
			src += sizeof(uint16_t);
			dest += sizeof(uint32_t);
			n -= sizeof(uint16_t) / sizeof(uint8_t);
		} else {
			const uint8_t *src_words  = src;
			uint16_t      *dest_words = dest;
			dest_words[0]		  = src_words[0];
			src += sizeof(uint8_t);
			dest += sizeof(uint8_t);
			n -= 1;
		}
	}
}

void
memcpy_shared2ram(void *__restrict__ dest, const void *__restrict__ src, size_t n)
{
	while (n > 0) {
		if (n >= sizeof(uint16_t)) {
			const uint32_t *src_words  = src;
			uint16_t       *dest_words = dest;
			dest_words[0]		   = src_words[0];
			src += sizeof(uint32_t);
			dest += sizeof(uint16_t);
			n -= sizeof(uint16_t) / sizeof(uint8_t);
		} else {
			const uint16_t *src_words  = src;
			uint8_t	       *dest_words = dest;
			dest_words[0]		   = src_words[0] & 0xff;
			src += sizeof(uint8_t);
			dest += sizeof(uint8_t);
			n -= 1;
		}
	}
}

static void
usb_endpoint_clear(uint8_t endp, bool clear_rx, bool clear_tx)
{
	usb_ep_t	    *usb_ep = GET_REG(USB_EP);
	volatile usb_epnr_t *reg    = &usb_ep->ep[endp];
	usb_epnr_t	     val    = {.bits = {.ctr_rx	 = !clear_rx,
						.ep_type = reg->bits.ep_type,
						.ep_kind = reg->bits.ep_kind,
						.ctr_tx	 = !clear_tx,
						.ea	 = reg->bits.ea}};
	REG_WRITE(&reg->val, val.val);
}

void
usb_endpoint_clear_rx(uint8_t endp)
{
	usb_endpoint_clear(endp, true, false);
}

void
usb_endpoint_clear_tx(uint8_t endp)
{
	usb_endpoint_clear(endp, false, true);
}

static void
usb_endpoint_state_rx(uint8_t endp, uint8_t stat)
{
	usb_ep_t	    *usb_ep = GET_REG(USB_EP);
	volatile usb_epnr_t *reg    = &usb_ep->ep[endp];
	usb_epnr_t	     val    = {.bits = {.ctr_rx	 = true,
						.stat_rx = reg->bits.stat_rx ^ stat,
						.ep_type = reg->bits.ep_type,
						.ep_kind = reg->bits.ep_kind,
						.ctr_tx	 = true,
						.ea	 = reg->bits.ea}};
	REG_WRITE(&reg->val, val.val);
}

static void
usb_endpoint_state_tx(uint8_t endp, uint8_t stat)
{
	usb_ep_t	    *usb_ep = GET_REG(USB_EP);
	volatile usb_epnr_t *reg    = &usb_ep->ep[endp];
	usb_epnr_t	     val    = {.bits = {
					   .ctr_rx  = true,
					   .ep_type = reg->bits.ep_type,
					   .ep_kind = reg->bits.ep_kind,
					   .ctr_tx  = true,
					   .stat_tx = reg->bits.stat_tx ^ stat,
					   .ea	    = reg->bits.ea,
			       }};
	REG_WRITE(&reg->val, val.val);
}

void
usb_endpoint_start_rx(uint8_t endp)
{
	usb_endpoint_state_rx(endp, USB_ENDPOINT_STATUS_VALID);
}

void
usb_endpoint_start_tx(uint8_t endp)
{
	usb_endpoint_state_tx(endp, USB_ENDPOINT_STATUS_VALID);
}

void
usb_endpoint_nack_rx(uint8_t endp)
{
	usb_endpoint_state_rx(endp, USB_ENDPOINT_STATUS_NAK);
}

void
usb_endpoint_nack_tx(uint8_t endp)
{
	usb_endpoint_state_tx(endp, USB_ENDPOINT_STATUS_NAK);
}

void
usb_endpoint_stall_rx(uint8_t endp)
{
	usb_endpoint_state_rx(endp, USB_ENDPOINT_STATUS_STALL);
}

void
usb_endpoint_stall_tx(uint8_t endp)
{
	usb_endpoint_state_tx(endp, USB_ENDPOINT_STATUS_STALL);
}

void
usb_endpoint_stop_rx(uint8_t endp)
{
	usb_endpoint_state_rx(endp, USB_ENDPOINT_STATUS_DISABLED);
}

void
usb_endpoint_stop_tx(uint8_t endp)
{
	usb_endpoint_state_tx(endp, USB_ENDPOINT_STATUS_DISABLED);
}

static void
usb_handle_reset(void)
{
	usb_global_state.state	       = USB_STATE_DEFAULT;
	usb_global_state.configuration = 0;
	usb_global_state.interface     = 0;
	usb_t	  *usb		       = GET_REG(USB);
	usb_istr_t rst		       = {.bits = {.reset = true}};
	REG_CLEAR(&usb->istr.val, rst.val);
	usb_btable_t btable = {.val = SHARED_SRAM_GLOBAL_TO_LOCAL((size_t) usb_btable)};
	REG_WRITE(&usb->btable.val, btable.val);
	usb->daddr.bits.add = 0;
	usb_ep_t *usb_ep    = GET_REG(USB_EP);
	for (size_t ep_idx = 0; ep_idx < usb_global_state.endpoint_count; ep_idx++) {
		usb_endpoint_state_t ep_state = usb_global_state.endpoints[ep_idx];
		usb_epnr_t	     epnr     = {.bits = {.ep_type = ep_state.endpoint_type, .ea = ep_state.address}};
		REG_WRITE(&usb_ep->ep[ep_idx].val, epnr.val);
		if (ep_state.tx_buffer)
			usb_btable[ep_idx].addr_tx.bits.addrn_tx = SHARED_SRAM_GLOBAL_TO_LOCAL(ep_state.tx_buffer);
		if (ep_state.rx_buffer)
			usb_btable[ep_idx].addr_rx.bits.addrn_rx = SHARED_SRAM_GLOBAL_TO_LOCAL(ep_state.rx_buffer);
		if (ep_state.rx_buffer_size) {
			if (ep_state.rx_buffer_size > 62) {
				usb_btable[ep_idx].count_rx.bits.blsize	   = true;
				usb_btable[ep_idx].count_rx.bits.num_block = ep_state.rx_buffer_size / 32 - 1;
			} else {
				usb_btable[ep_idx].count_rx.bits.blsize	   = false;
				usb_btable[ep_idx].count_rx.bits.num_block = ep_state.rx_buffer_size / 2;
			}
		}
	}
	usb_endpoint_start_rx(0);
	usb_endpoint_nack_tx(0);
	usb_daddr_t daddr = {.bits = {.ef = true}};
	REG_SET(&usb->daddr.val, daddr.val);
}

static void
usb_handle_endpoint0_setup_get_descriptor(usb_setup_packet_shared_t *packet)
{
	usb_endpoint_state_t *state = &usb_global_state.endpoints[0];
	switch ((usb_descriptor_t) packet->DescriptorType) {
	case USB_DESCRIPTOR_DEVICE: {
		_Static_assert(USB_EP0_SIZE >= sizeof(device_descriptor),
			       "USB endpoint 0 buffer size not big enough for "
			       "device_descriptor_t");
		usb_endpoint_transfer_single(0, &device_descriptor, MIN(sizeof(device_descriptor), packet->wLength));
		usb_endpoint_start_tx(0);
		return;
	}
	case USB_DESCRIPTOR_DEVICE_QUALIFIER: {
		usb_endpoint_start_rx(0);
		usb_endpoint_stall_tx(0);
		return;
	}
	case USB_DESCRIPTOR_CONFIGURATION: {
		usb_endpoint_prepare_chunked(&usb_full_configuraion_descriptor_buffer);
		usb_global_state.endpoints[0].tx_expected_size = packet->wLength;
		if (usb_endpoint_transfer_chunked_begin(0, &usb_full_configuraion_descriptor_buffer)) {
			state->send_chunks   = &usb_full_configuraion_descriptor_buffer;
			state->transmit_type = USB_TRANSMIT_TYPE_CHUNKED;
			usb_endpoint_start_tx(0);
		} else {
			usb_endpoint_stall_tx(0);
		}
		return;
	}
	case USB_DESCRIPTOR_STRING: {
		if (packet->DescriptorIndex == 0) {
			uint16_t descriptor_size =
			    sizeof(uint8_t) * 2 + usb_string_list.language_count * sizeof(uint16_t);
			uint8_t			    buffer[descriptor_size];
			usb_descriptor_languages_t *langs = (void *) buffer;
			langs->bDescriptorType		  = USB_DESCRIPTOR_STRING;
			langs->bLength			  = descriptor_size;
			for (size_t lang_idx = 0; lang_idx < usb_string_list.language_count; lang_idx++) {
				langs->wLangID[lang_idx] = usb_string_list.langs[lang_idx]->wLangID;
			}
			usb_endpoint_transfer_single(0, buffer, MIN(descriptor_size, packet->wLength));
			usb_endpoint_start_tx(0);
		} else {
			usb_string_list_item_t *lang_list = NULL;
			for (size_t lang_idx = 0; lang_idx < usb_string_list.language_count; lang_idx++) {
				if (usb_string_list.langs[lang_idx]->wLangID == packet->wIndex)
					lang_list = usb_string_list.langs[lang_idx];
			}
			if ((lang_list == NULL) ||
			    (USB_STRING_NUM_TO_IDX(packet->DescriptorIndex) >= lang_list->string_count)) {
				usb_endpoint_start_rx(0);
				usb_endpoint_stall_tx(0);
				return;
			}
			usb_global_state.endpoints[0].tx_expected_size = packet->wLength;
			wchar_t *string = lang_list->strings[USB_STRING_NUM_TO_IDX(packet->DescriptorIndex)];
			usb_endpoint_transfer_string_descriptor_begin(0, string);
			usb_endpoint_start_tx(0);
		}
		return;
	}
	default:
		return;
	}
}

static void
usb_handle_endpoint0_setup()
{
	usb_ep_t  *usb_ep = GET_REG(USB_EP);
	usb_epnr_t ep_r	  = usb_ep->ep[0];
	if ((ep_r.bits.ctr_rx) && (usb_btable[0].count_rx.bits.countn_rx == sizeof(usb_setup_packet_t))) {
		usb_setup_packet_shared_t *packet = (void *) &usb_buffer_ep0_rx;
		if ((usb_setup_type_t) packet->Type == USB_SETUP_TYPE_STANDARD) {
			switch ((usb_request_t) packet->bRequest) {
			case USB_REQUEST_CLEAR_FEATURE: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
				    ((usb_feature_t) packet->wValue == USB_FEATURE_ENDPOINT_HALT) &&
				    ((usb_setup_recipient_t) packet->Recipient == USB_SETUP_RECIPIENT_ENDPOINT) &&
				    ((packet->wIndex == 0) || (usb_global_state.state == USB_STATE_CONFIGURED))) {
					int8_t ep_idx;
					if ((ep_idx = usb_endpoint_address_to_index(packet->wIndex)) == -1) {
						usb_endpoint_stall_tx(0);
					} else {
						usb_global_state.endpoints[ep_idx].halt = false;
						if (usb_global_state.endpoints[ep_idx].rx_buffer)
							usb_endpoint_start_rx(ep_idx);
						if (usb_global_state.endpoints[ep_idx].tx_buffer)
							usb_endpoint_nack_tx(ep_idx);
						if (usb_global_state.endpoints[ep_idx].init_callback)
							usb_global_state.endpoints[ep_idx].init_callback(ep_idx);
						usb_endpoint_transfer_zero_size(0);
						usb_endpoint_start_tx(0);
					}
					return;
				}
				break;
			}
			case USB_REQUEST_GET_CONFIGURATION: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
				    (usb_global_state.state != USB_STATE_DEFAULT)) {
					uint8_t buffer[] = {usb_global_state.configuration};
					usb_endpoint_transfer_single(0, buffer, sizeof(buffer));
					usb_endpoint_start_tx(0);
					return;
				}
				break;
			}
			case USB_REQUEST_GET_DESCRIPTOR: {
				if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) {
					usb_handle_endpoint0_setup_get_descriptor(packet);
					return;
				}
				break;
			}
			case USB_REQUEST_GET_INTERFACE: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
				    (usb_global_state.state == USB_STATE_CONFIGURED)) {
					uint8_t buffer[] = {usb_global_state.interface};
					usb_endpoint_transfer_single(0, buffer, sizeof(buffer));
					usb_endpoint_start_tx(0);
					return;
				}
				break;
			}
			case USB_REQUEST_GET_STATUS: {
				if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) {
					if (((usb_global_state.state == USB_STATE_ADDRESS) && (packet->wValue == 0)) ||
					    (usb_global_state.state == USB_STATE_CONFIGURED)) {
						uint8_t buffer[2] = {0, 0};
						usb_endpoint_transfer_single(0, buffer, sizeof(buffer));
						usb_endpoint_start_tx(0);
						return;
					}
				}
				break;
			}
			case USB_REQUEST_SET_ADDRESS: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
				    (((usb_global_state.state == USB_STATE_DEFAULT) && (packet->wValue != 0)) ||
				     ((usb_global_state.state == USB_STATE_ADDRESS) && (packet->wValue == 0)))) {
					usb_global_state.endpoints[0].function_address = packet->wValue;
					usb_global_state.endpoints[0].transmit_type    = USB_TRANSMIT_TYPE_ADDRESS;
					usb_endpoint_transfer_zero_size(0);
					usb_endpoint_start_tx(0);
					return;
				}
				break;
			}
			case USB_REQUEST_SET_CONFIGURATION: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
				    (((usb_global_state.state == USB_STATE_ADDRESS) && (packet->wValue != 0)) ||
				     ((usb_global_state.state == USB_STATE_CONFIGURED) && (packet->wValue == 0)))) {
					usb_global_state.configuration = packet->wValue;
					usb_global_state.state =
					    packet->wValue ? USB_STATE_CONFIGURED : USB_STATE_ADDRESS;
					for (size_t ep_idx = 1; ep_idx < usb_global_state.endpoint_count; ep_idx++) {
						if (packet->wValue) {
							if (usb_global_state.endpoints[ep_idx].rx_buffer)
								usb_endpoint_start_rx(ep_idx);
							if (usb_global_state.endpoints[ep_idx].tx_buffer)
								usb_endpoint_nack_tx(ep_idx);
							if (usb_global_state.endpoints[ep_idx].init_callback)
								usb_global_state.endpoints[ep_idx].init_callback(
								    ep_idx);
						} else {
							usb_endpoint_stop_rx(ep_idx);
							usb_endpoint_stop_tx(ep_idx);
						}
					}
					usb_endpoint_transfer_zero_size(0);
					usb_endpoint_start_tx(0);
					return;
				}
				break;
			}
			case USB_REQUEST_SET_DESCRIPTOR: {
				break;
			}
			case USB_REQUEST_SET_FEATURE: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
				    ((usb_feature_t) packet->wValue == USB_FEATURE_ENDPOINT_HALT) &&
				    ((usb_setup_recipient_t) packet->Recipient == USB_SETUP_RECIPIENT_ENDPOINT) &&
				    ((packet->wIndex == 0) || (usb_global_state.state == USB_STATE_CONFIGURED))) {
					int8_t ep_idx;
					if ((ep_idx = usb_endpoint_address_to_index(packet->wIndex)) == -1) {
						usb_endpoint_stall_tx(0);
					} else {
						usb_global_state.endpoints[ep_idx].halt = true;
						usb_endpoint_stall_rx(ep_idx);
						usb_endpoint_stall_tx(ep_idx);
						usb_endpoint_transfer_zero_size(0);
						usb_endpoint_start_tx(0);
					}
					return;
				}
				break;
			}
			case USB_REQUEST_SET_INTERFACE: {
				if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
				    (usb_global_state.state == USB_STATE_CONFIGURED) && (packet->wValue != 0)) {
					usb_global_state.interface = packet->wValue;
					usb_endpoint_transfer_zero_size(0);
					usb_endpoint_start_tx(0);
					return;
				}
				break;
			}
			case USB_REQUEST_SYNCH_FRAME: {
				break;
			}
			}
		} else {
			void (*rx_callback)(uint8_t) = usb_global_state.endpoints[0].rx_callback;
			if (rx_callback) {
				rx_callback(0);
				return;
			}
		}
	}
	usb_endpoint_start_rx(0);
	usb_endpoint_stall_tx(0);
}

void
usb_irqhandler()
{
	usb_t	  *usb	  = GET_REG(USB);
	usb_istr_t status = {.val = usb->istr.val};
	REG_WRITE(&usb->istr.val, 0); // clear spurious interrupts
	if (status.bits.reset) {
		usb_handle_reset();
	} else if (status.bits.ctr) {
		usb_ep_t *usb_ep = GET_REG(USB_EP);
		int	  ep_idx;
		if ((ep_idx = usb_endpoint_address_to_index(status.bits.ep_id)) == -1)
			return;
		usb_epnr_t ep_r = usb_ep->ep[ep_idx];
		if (status.bits.dir) {
			if (usb_global_state.endpoints[ep_idx].halt) {
				usb_endpoint_stall_rx(ep_idx);
			} else if ((ep_idx == 0) && ep_r.bits.setup) {
				usb_handle_endpoint0_setup();
			} else {
				void (*rx_callback)(uint8_t) = usb_global_state.endpoints[ep_idx].rx_callback;
				if (rx_callback)
					rx_callback(ep_idx);
			}
			usb_endpoint_clear_rx(ep_idx);
		} else {
			if (ep_idx == 0) {
				usb_endpoint_state_t *state = &usb_global_state.endpoints[0];
				switch ((usb_transmit_type_t) state->transmit_type) {
				case USB_TRANSMIT_TYPE_NONE: {
					usb_endpoint_start_rx(0);
					break;
				}
				case USB_TRANSMIT_TYPE_CHUNKED: {
					if (usb_endpoint_transfer_chunked_continue(0)) {
						usb_endpoint_start_tx(0);
					} else {
						usb_endpoint_start_rx(0);
					}
					break;
				}
				case USB_TRANSMIT_TYPE_STRING: {
					if (usb_endpoint_transfer_string_continue(0)) {
						usb_endpoint_start_tx(0);
					} else {
						usb_endpoint_start_rx(0);
					}
					break;
				}
				case USB_TRANSMIT_TYPE_UTF8_STRING: {
					if (usb_endpoint_transfer_string_utf8_continue(0)) {
						usb_endpoint_start_tx(0);
					} else {
						usb_endpoint_start_rx(0);
					}
					break;
				}
				case USB_TRANSMIT_TYPE_BUFFER: {
					if (usb_endpoint_transfer_buffer_continue(0)) {
						usb_endpoint_start_tx(0);
					} else {
						usb_endpoint_start_rx(0);
					}
					break;
				}
				case USB_TRANSMIT_TYPE_ADDRESS: {
					usb_daddr_t addr = {.bits = {.ef = true, .add = state->function_address}};
					REG_WRITE(&usb->daddr.val, addr.val);
					usb_global_state.state =
					    state->function_address == 0 ? USB_STATE_DEFAULT : USB_STATE_ADDRESS;
					usb_endpoint_start_rx(0);
					break;
				}
				}
			} else {
				void (*tx_callback)(uint8_t) = usb_global_state.endpoints[ep_idx].tx_callback;
				if (tx_callback)
					tx_callback(ep_idx);
			}
			usb_endpoint_clear_tx(ep_idx);
		}
	} else {
		BKPT;
	}
}

void
usb_handle_transfer(uint8_t ep)
{
	usb_endpoint_state_t *state = &usb_global_state.endpoints[ep];
	switch ((usb_transmit_type_t) state->transmit_type) {
	case USB_TRANSMIT_TYPE_NONE: {
		usb_endpoint_start_rx(ep);
		break;
	}
	case USB_TRANSMIT_TYPE_CHUNKED: {
		if (usb_endpoint_transfer_chunked_continue(ep)) {
			usb_endpoint_start_tx(ep);
		} else {
			usb_endpoint_start_rx(ep);
		}
		break;
	}
	case USB_TRANSMIT_TYPE_STRING: {
		if (usb_endpoint_transfer_string_continue(ep)) {
			usb_endpoint_start_tx(ep);
		} else {
			usb_endpoint_start_rx(ep);
		}
		break;
	}
	case USB_TRANSMIT_TYPE_UTF8_STRING: {
		if (usb_endpoint_transfer_string_utf8_continue(ep)) {
			usb_endpoint_start_tx(ep);
		} else {
			usb_endpoint_start_rx(ep);
		}
		break;
	}
	case USB_TRANSMIT_TYPE_BUFFER: {
		if (usb_endpoint_transfer_buffer_continue(ep)) {
			usb_endpoint_start_tx(ep);
		} else {
			usb_endpoint_start_rx(ep);
		}
		break;
	}
	default: {
		break;
	}
	}
}
