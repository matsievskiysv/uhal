# toolchain
CROSS_COMPILE := arm-none-eabi-
CC := $(CROSS_COMPILE)gcc
AS := $(CROSS_COMPILE)as
AR := $(CROSS_COMPILE)ar
NEWLIB_INCLUDE ?= /usr/lib/arm-none-eabi/include

CHIP ?=

AARCH := -mcpu=cortex-m3
CCWARNINGS := -Wall -Wextra
ASWARNINGS := --fatal-warnings
ARMMODE := -mthumb
ABI := -mabi=aapcs-linux

# flags
GCCOPTS := -nostartfiles -ffreestanding -nodefaultlibs -fno-zero-initialized-in-bss -fstack-usage
CCFLAGS := -std=gnu2x $(GCCOPTS) $(ARMMODE) $(WARNINGS) $(AARCH) $(ABI)
INCLUDE := -Iinclude -I$(NEWLIB_INCLUDE)

CCFLAGS += -D$(CHIP)

ifdef FPU
ABI += -mfloat-abi=soft
CCFLAGS += -D__FPU_PRESENT
else
endif

ifndef NDEBUG
CCFLAGS += -O0 -ggdb
ASFLAGS += -gstabs+ -g
else
CCFLAGS += -Os
endif

# objects
TARGET := lib$(CHIP).a
SRC := $(wildcard *.c *.s)
OBJS := $(addprefix build/,$(patsubst %.s,%.o,$(SRC:%.c=%.o)))
INCLUDE_FILES := $(wildcard include/*.h stm32f10x/*.h stm32f103m/*.h stm32f103h/*.h)

$(TARGET): $(OBJS)
	$(AR) -rcs $@ $(OBJS)

build:
	mkdir -p build

build/%.o: %.s | build
	$(AS) -c $(ASFLAGS) $(ASWARNINGS) $< -o $@

build/%.o: %.c $(INCLUDE_FILES) | build
	$(CC) -c $(INCLUDE) $(CCFLAGS) $(CCWARNINGS) $< -o $@

.PHONY: indent
indent:
	clang-format -i --style=file $(SRC) $(INCLUDE_FILES)

.PHONY: clean
clean:
	rm -rf build
	rm -f *.o
	rm -f *.a
	rm -f *.log
	rm -f *~
