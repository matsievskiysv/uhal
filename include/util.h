#pragma once

#include <stddef.h>

/**
 * @brief Set the debugger breakpoint.
 */
#define BKPT do {__asm__("BKPT");} while (0)

/**
 * @brief Minimum of two values.
 */
#define MIN(x, y) ((x) < (y) ? (x) : (y))

/**
 * @brief Maximum of two values.
 */
#define MAX(x, y) ((x) > (y) ? (x) : (y))

/**
 * @brief Get size of array.
 */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

/**
 * @brief Internal register type.
 */
#define REG_T volatile size_t *const

/**
 * @brief Get value with bit \p X set to 1.
 */
#define BIT(X) (1 << X)

/**
 * @brief Get register pointer from \p BASE address.
 */
#define GET_REG(BASE) ((void *) (BASE))

/**
 * @brief Get register pointer from \p BASE address and \p OFFSET.
 */
#define GET_REG_OFFSET(BASE, OFFSET) ((void *) ((BASE) + (OFFSET)))

/**
 * @brief Access register value.
 */
#define REG_VAL(REG) (*REG)

/**
 * @brief Write value to \p REG.
 */
#define REG_WRITE(REG, VAL) *REG = (VAL)

/**
 * @brief Write value to \p REG with MASK.
 */
#define REG_WRITE_MASK(REG, VAL, MASK) *REG = ((REG_VAL(REG) | ((VAL) & (MASK))) & ~(~(VAL) & (MASK)))

/**
 * @brief Set bits in \p REG from \p MASK to 1.
 */
#define REG_SET(REG, MASK) *REG |= (MASK)

/**
 * @brief Set bits in \p REG from \p MASK to 0.
 */
#define REG_CLEAR(REG, MASK) *REG &= ~(MASK)

/**
 * @brief Toggle bits in \p REG from \p MASK.
 */
#define REG_TOGGLE(REG, MASK) *REG ^= (MASK)

/**
 * @brief Wait until \p COND is false.
 */
#define WAIT_WHILE(COND)                                                                                               \
	while (COND) {}

/**
 * @brief Loop indefinitely.
 */
#define LOOP(BODY)                                                                                                     \
	for (;;) {                                                                                                     \
		BODY                                                                                                   \
	}
