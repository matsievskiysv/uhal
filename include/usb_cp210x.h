#pragma once

#include "usb.h"

typedef enum {
	USB_CP210X_DATA_BUFFER_SIZE_32 = 32,
	USB_CP210X_DATA_BUFFER_SIZE_64 = 64,
} usb_cp210x_data_buffer_size_t;

/**
 * @brief USB device vendor ID for CP210X.
 */
#define USB_CP210X_ID_VENDOR 0x10c4

/**
 * @brief USB device vendor ID for CP210X.
 */
#define USB_CP210X_ID_PRODUCT 0xea60

/**
 * @brief USB device class used for CP210X.
 */
#define USB_CP210X_DEVICE_CLASS 0x0

/**
 * @brief USB device subclass used for CP210X.
 */
#define USB_CP210X_DEVICE_SUBCLASS 0x0

/**
 * @brief USB device protocol used for CP210X.
 */
#define USB_CP210X_DEVICE_PROTOCOL 0x0

/**
 * @brief USB interface class used for CP210X.
 */
#define USB_CP210X_INTERFACE_CLASS 0xff

/**
 * @brief USB interface subclass used for CP210X.
 */
#define USB_CP210X_INTERFACE_SUBCLASS 0x0

/**
 * @brief USB interface protocol used for CP210X.
 */
#define USB_CP210X_INTERFACE_PROTOCOL 0x0

/**
 * @brief USB CP210X class request.
 */
typedef enum {
	USB_CP210X_REQUEST_IFC_ENABLE	   = 0x00,
	USB_CP210X_REQUEST_SET_BAUDDIV	   = 0x01,
	USB_CP210X_REQUEST_GET_BAUDDIV	   = 0x02,
	USB_CP210X_REQUEST_SET_LINE_CTL	   = 0x03,
	USB_CP210X_REQUEST_GET_LINE_CTL	   = 0x04,
	USB_CP210X_REQUEST_SET_BREAK	   = 0x05,
	USB_CP210X_REQUEST_IMM_CHAR	   = 0x06,
	USB_CP210X_REQUEST_SET_MHS	   = 0x07,
	USB_CP210X_REQUEST_GET_MDMSTS	   = 0x08,
	USB_CP210X_REQUEST_SET_XON	   = 0x09,
	USB_CP210X_REQUEST_SET_XOFF	   = 0x0A,
	USB_CP210X_REQUEST_SET_EVENTMASK   = 0x0B,
	USB_CP210X_REQUEST_GET_EVENTMASK   = 0x0C,
	USB_CP210X_REQUEST_GET_EVENTSTATE  = 0x16,
	USB_CP210X_REQUEST_SET_RECEIVE	   = 0x17,
	USB_CP210X_REQUEST_GET_RECEIVE	   = 0x18,
	USB_CP210X_REQUEST_SET_CHAR	   = 0x0D,
	USB_CP210X_REQUEST_GET_CHARS	   = 0x0E,
	USB_CP210X_REQUEST_GET_PROPS	   = 0x0F,
	USB_CP210X_REQUEST_GET_COMM_STATUS = 0x10,
	USB_CP210X_REQUEST_RESET	   = 0x11,
	USB_CP210X_REQUEST_PURGE	   = 0x12,
	USB_CP210X_REQUEST_SET_FLOW	   = 0x13,
	USB_CP210X_REQUEST_GET_FLOW	   = 0x14,
	USB_CP210X_REQUEST_EMBED_EVENTS	   = 0x15,
	USB_CP210X_REQUEST_GET_BAUDRATE	   = 0x1D,
	USB_CP210X_REQUEST_SET_BAUDRATE	   = 0x1E,
	USB_CP210X_REQUEST_SET_CHARS	   = 0x19,
	USB_CP210X_REQUEST_VENDOR_SPECIFIC = 0XFF,
} usb_cp210x_request_t;

/**
 * @brief USB CP210X class vendor specific request.
 */
typedef enum {
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_GET_FW_VER	  = 0x000E,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_READ_2NCONFIG  = 0x000E,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_GET_FW_VER_2N  = 0x0010,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_READ_LATCH	  = 0x00C2,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_GET_PARTNUM	  = 0x370B,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_GET_PORTCONFIG = 0x370C,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_GET_DEVICEMODE = 0x3711,
	USB_CP210X_VENDOR_SPECIFIC_REQUEST_WRITE_LATCH	  = 0x37E1,
} usb_cp210x_vendor_specific_request_t;

#define USB_CP210X_CP2101_PARTNUM 0x01

/**
 * @brief USB serial line coding.
 */
typedef enum {
	USB_SERIAL_CHAR_1   = 0,
	USB_SERIAL_CHAR_1_5 = 1,
	USB_SERIAL_CHAR_2   = 2,
} usb_cdc_line_coding_char_t;

/**
 * @brief USB serial line parity.
 */
typedef enum {
	USB_SERIAL_PARITY_NONE	= 0,
	USB_SERIAL_PARITY_ODD	= 1,
	USB_SERIAL_PARITY_EVEN	= 2,
	USB_SERIAL_PARITY_MARK	= 3,
	USB_SERIAL_PARITY_SPACE = 4,
} usb_cdc_line_coding_parity_t;

/**
 * @brief USB serial line number of bits.
 */
typedef enum {
	USB_SERIAL_DATA_BITS_5 = 5,
	USB_SERIAL_DATA_BITS_6 = 6,
	USB_SERIAL_DATA_BITS_7 = 7,
	USB_SERIAL_DATA_BITS_8 = 8,
} usb_cdc_line_coding_data_bits_t;

typedef union {
	struct __attribute__((packed)) {
		uint8_t stop_bits : 4;
		uint8_t parity	  : 4;
		uint8_t word_length;
	} bits;
	uint16_t val;
} usb_cp210x_line_ctl_t;
_Static_assert(sizeof(usb_cp210x_line_ctl_t) == sizeof(uint16_t), "invalid usb_cp210x_line_ctl_t size");

typedef union {
	struct __attribute__((packed)) {
		uint8_t serial_dtr_mask	    : 2;
		bool			    : 1;
		bool serial_cts_handshake   : 1;
		bool serial_dsr_handshake   : 1;
		bool serial_dsd_handshake   : 1;
		bool serial_dsr_sensitivity : 1;
	} bits;
	uint32_t val;
} usb_cp210x_ul_control_handshake_t;
_Static_assert(sizeof(usb_cp210x_ul_control_handshake_t) == sizeof(uint32_t),
	       "invalid usb_cp210x_ul_control_handshake_t size");

typedef union {
	struct __attribute__((packed)) {
		bool serial_auto_transmit  : 1;
		bool serial_auto_receive   : 1;
		bool serial_error_char	   : 1;
		bool serial_null_stripping : 1;
		bool serial_break_char	   : 1;
		bool			   : 1;
		bool			   : 1;
		bool serial_rts_mask_lo	   : 1;
		bool serial_rts_mask_hi	   : 1;
		uint8_t			   : 7;
		uint16_t		   : 15;
		bool serial_xoff_continue  : 1;
	} bits;
	uint32_t val;
} usb_cp210x_ul_flow_replace_t;
_Static_assert(sizeof(usb_cp210x_ul_flow_replace_t) == sizeof(uint32_t), "invalid usb_cp210x_ul_flow_replace_t size");

typedef struct __attribute__((packed)) {
	usb_cp210x_ul_control_handshake_t ulControlHandshake;
	usb_cp210x_ul_flow_replace_t	  ulFlowReplace;
	uint32_t			  ulXonLimit;
	uint32_t			  ulXoffLimit;
} usb_cp210x_flow_control_t;
_Static_assert(sizeof(usb_cp210x_flow_control_t) == sizeof(uint8_t) * 16, "invalid usb_cp210x_flow_control_t size");

typedef union {
	struct __attribute__((packed)) {
		bool dtr_state : 1;
		bool rts_state : 1;
		uint8_t	       : 6;
		bool dtr_mask  : 1;
		bool rts_mask  : 1;
	} bits;
	uint16_t val;
} usb_cp210x_mhs_t;
_Static_assert(sizeof(usb_cp210x_mhs_t) == sizeof(uint16_t), "invalid usb_cp210x_mhs_t size");

typedef struct __attribute__((packed)) {
	union {
		uint32_t ulErrors;
		struct {
			bool break_event      : 1;
			bool framing_error    : 1;
			bool hardware_overrun : 1;
			bool queue_overrun    : 1;
			bool parity_error     : 1;
		};
	};
	union {
		uint32_t ulHoldReasons;
		struct {
			bool wait_cts	  : 1;
			bool wait_dsr	  : 1;
			bool wait_dsd	  : 1;
			bool wait_xon	  : 1;
			bool wait_xoff	  : 1;
			bool wait_break	  : 1;
			bool wait_dsr_rcv : 1;
		};
	};
	uint32_t ulAmountInInQueue;
	uint32_t ulAmountInOutQueue;
	bool	 bEofReceived;
	bool	 bWaitForImmediate;
	uint8_t : 8;
} usb_cp210x_serial_status_t;
_Static_assert(sizeof(usb_cp210x_serial_status_t) == sizeof(uint8_t) * 19, "invalid usb_cp210x_serial_status_t size");

typedef union {
	struct __attribute__((packed)) {
		bool dtr : 1;
		bool rts : 1;
		uint8_t	 : 2;
		bool cts : 1;
		bool dsr : 1;
		bool ri	 : 1;
		bool dcd : 1;
	} bits;
	uint8_t val;
} usb_cp210x_mdmsts_t;
_Static_assert(sizeof(usb_cp210x_mdmsts_t) == sizeof(uint8_t), "invalid usb_cp210x_mdmsts_t size");

typedef union {
	struct __attribute__((packed)) {
		bool ri_trailing_edge_occurred	: 1;
		bool				: 1;
		bool rcv_buf_80pct_full		: 1;
		uint8_t				: 5;
		bool char_recieved		: 1;
		bool special_char_received	: 1;
		bool transmit_queue_empty	: 1;
		bool cts_changed		: 1;
		bool dsr_changed		: 1;
		bool dsd_changed		: 1;
		bool line_break_recieved	: 1;
		bool line_status_error_occurred : 1;
	} bits;
	uint8_t val;
} usb_cp210x_event_t;
_Static_assert(sizeof(usb_cp210x_event_t) == sizeof(uint16_t), "invalid usb_cp210x_event_t size");

typedef struct __attribute__((packed)) {
	uint16_t wLength;
	uint16_t bcdVersion;
	uint32_t ulServiceMask;
	uint32_t : 32;
	uint32_t ulMaxTxQueue;
	uint32_t ulMaxRxQueue;
	uint32_t ulMaBaud;
	union {
		uint32_t ulProvSubType;
		struct {
			bool kind_unspecified : 1;
			bool kind_rs232	      : 1;
			uint8_t		      : 4;
			bool kind_modem_ta    : 1;
		};
	};
	union {
		uint32_t ulProvCapabilities;
		struct {
			bool dtr_dsr_support		 : 1;
			bool rts_cts_support		 : 1;
			bool dcd_support		 : 1;
			bool can_check_parity		 : 1;
			bool xon_xoff_support		 : 1;
			bool can_set_xon_xoff_characters : 1;
			bool				 : 1;
			bool				 : 1;
			bool can_set_special_characters	 : 1;
			bool bit16_mode_supports	 : 1;
		};
	};
	union {
		uint32_t ulSettableParams;
		struct {
			bool can_set_parity_type	     : 1;
			bool can_set_baud		     : 1;
			bool can_set_number_of_data_bits     : 1;
			bool can_set_stop_bits		     : 1;
			bool can_set_handshaking	     : 1;
			bool can_set_parity_checking	     : 1;
			bool can_set_carrier_detect_checking : 1;
		};
	};
	union {
		uint32_t ulSettableBaud;
		struct {
			bool baud_75	 : 1;
			bool baud_110	 : 1;
			bool baud_134_5	 : 1;
			bool baud_150	 : 1;
			bool baud_300	 : 1;
			bool baud_600	 : 1;
			bool baud_1200	 : 1;
			bool baud_1800	 : 1;
			bool baud_2400	 : 1;
			bool baud_4800	 : 1;
			bool baud_7200	 : 1;
			bool baud_9600	 : 1;
			bool baud_14400	 : 1;
			bool baud_19200	 : 1;
			bool baud_38400	 : 1;
			bool baud_56000	 : 1;
			bool baud_128000 : 1;
			bool baud_115200 : 1;
			bool baud_57600	 : 1;
		};
	};
	union {
		uint32_t wSettableData;
		struct {
			bool data_bits_5	   : 1;
			bool data_bits_6	   : 1;
			bool data_bits_7	   : 1;
			bool data_bits_8	   : 1;
			bool data_bits_16	   : 1;
			bool data_bits_16_extended : 1;
		};
	};
	uint32_t ulCurrentTxQueue;
	uint32_t ulCurrentRxQueue;
	uint32_t : 32;
	uint32_t : 32;
	char uniProvName[15];
} usb_cp210x_props_t;
_Static_assert(sizeof(usb_cp210x_props_t) == sizeof(uint8_t) * 75, "invalid usb_cp210x_props_t size");

typedef enum {
	USB_CP210X_CONTROL_CHARS_EOF   = 0,
	USB_CP210X_CONTROL_CHARS_ERROR = 1,
	USB_CP210X_CONTROL_CHARS_BREAK = 2,
	USB_CP210X_CONTROL_CHARS_EVENT = 3,
	USB_CP210X_CONTROL_CHARS_XON   = 4,
	USB_CP210X_CONTROL_CHARS_XOFF  = 5,
	USB_CP210X_CONTROL_CHARS_MAX,
} usb_cp210x_control_chars_t;

typedef struct {
	uint32_t		   baudrate;
	usb_cp210x_line_ctl_t	   line_ctl;
	usb_cp210x_flow_control_t  flow_control;
	usb_cp210x_mhs_t	   mhs;
	usb_cp210x_serial_status_t serial_status;
	char			   control_chars[USB_CP210X_CONTROL_CHARS_MAX];
	usb_cp210x_mdmsts_t	   mdmsts;
	bool			   xon;
	bool			   xoff;
	usb_cp210x_props_t	   props;
	struct {
		usb_cp210x_event_t mask;
		usb_cp210x_event_t state;
	} event;
	union {
		struct {
			bool setting_baudrate	   : 1;
			bool setting_control_chars : 1;
			bool setting_flow	   : 1;
		};
		uint8_t setting_in_progress;
	};
} usb_cp210x_global_state_t;

/**
 * @brief USB CP210X global state instance.
 */
extern usb_cp210x_global_state_t usb_cp210x_global_state;

/**
 * @brief USB CP210X endpoint setup callback.
 */
void usb_handle_cp210x_setup(uint8_t ep);
