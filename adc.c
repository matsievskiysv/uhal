#include <adc.h>
#include <util.h>

void
adc_temp_config(bool int_enable, bool dma_enable)
{
	adc_t	 *adc	   = GET_REG(ADC1);
	adc_cr1_t cr1_mask = {.bits = {.eocie = true, .scan = true}};
	adc_cr1_t cr1_val  = {.bits = {.eocie = int_enable, .scan = true}};
	REG_WRITE_MASK(&adc->cr1.val, cr1_val.val, cr1_mask.val);
	adc_cr2_t cr2_mask = {.bits = {.dma = true, .tsvrefe = true, .adon = true}};
	adc_cr2_t cr2_val  = {.bits = {.dma = dma_enable, .tsvrefe = true, .adon = true}};
	REG_WRITE_MASK(&adc->cr2.val, cr2_val.val, cr2_mask.val);
	// set maximum sample time for temp and ref channels
	adc_smpr2_t smpr2 = {.bits = {.smp6 = ADC_SMP_239_5, .smp7 = ADC_SMP_239_5}};
	REG_SET(&adc->smpr2.val, smpr2.val);
	// set two channels
	adc_sqr1_t sqr1_mask = {.bits = {.l = ADC_L_16}};
	adc_sqr1_t sqr1_val  = {.bits = {.l = ADC_L_2}};
	REG_WRITE_MASK(&adc->sqr1.val, sqr1_val.val, sqr1_mask.val);
	// set temp and ref channels
	adc_sqr3_t sqr3_mask = {.bits = {.sq1 = 0b11111, .sq2 = 0b11111}};
	adc_sqr3_t sqr3_val  = {.bits = {.sq1 = 16, .sq2 = 17}};
	REG_WRITE_MASK(&adc->sqr3.val, sqr3_val.val, sqr3_mask.val);
}

void
adc_calibrate(size_t base)
{
	adc_t *adc = GET_REG(base);
	// reset
	{
		adc_cr2_t cr2 = {.bits = {.rstcal = true}};
		REG_SET(&adc->cr2.val, cr2.val);
	}
	// calibrate
	{
		adc_cr2_t cr2 = {.bits = {.cal = true}};
		REG_SET(&adc->cr2.val, cr2.val);
	}
	WAIT_WHILE(adc->cr2.bits.cal);
}

void
adc_temp_start(void)
{
	adc_t *adc = GET_REG(ADC1);
	// clear status register
	adc_sr_t sr = {.bits = {.awd = true, .eoc = true, .jeoc = true, .jstrt = true, .strt = true}};
	REG_CLEAR(&adc->sr.val, sr.val);
	adc_cr2_t cr2 = {.bits = {.adon = true}};
	REG_SET(&adc->cr2.val, cr2.val);
}

int32_t
adc_temp_convert(int16_t temp_code, int16_t ref_code)
{
#ifdef __FPU_PRESENT
	return (1.43 - (1.2 * temp_code) / ref_code) / 0.0043 * 1000 + 25000;
#else
	return (14300 - (12000L * temp_code) / ref_code) * 1000 / 43 + 25000;
#endif
}
