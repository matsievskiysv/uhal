#include <flash.h>
#include <util.h>

void
flash_latency(uint8_t latency)
{
	flash_t *flash = GET_REG(FLASH);
	flash_acr_t acr_val = {.bits = {.latency = latency}};
	flash_acr_t acr_mask = {.bits = {.latency = 0b111}};
	REG_WRITE_MASK(&flash->acr.val, acr_val.val, acr_mask.val);
}
