#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/dma.h"
#elif defined(set32f103h)
#include "../stm32f103h/dma.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

#include <stdbool.h>
#include <stdint.h>

#define DMA_PRIORITY_LOW       DMA_PL_LO
#define DMA_PRIORITY_MEDIUM    DMA_PL_MED
#define DMA_PRIORITY_HIGH      DMA_PL_HI
#define DMA_PRIORITY_VERY_HIGH DMA_PL_VHI

typedef union {
	struct __attribute((packed)) {
		bool global		    : 1;
		bool transfer_complete	    : 1;
		bool half_transfer_complete : 1;
		bool transfer_error	    : 1;
	} bits;
	size_t val;
} dma_interrupt_t;

/**
 * @brief Configure DMA channel.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @param[in] mem Memory address.
 * @param[in] phr Peripheral address.
 * @param[in] size Transmission count.
 * @param[in] mem2mem Memory to memory transmission mode.
 * @param[in] level Priority level.
 * @param[in] mem_sz Memory chunk size.
 * @param[in] phr_sz Peripheral chunk size.
 * @param[in] mem_inc Memory increment mode.
 * @param[in] phr_inc Peripheral increment mode.
 * @param[in] circ Circular transmission mode.
 * @param[in] mem2phr Transmission direction from memory to peripheral.
 * @param[in] err_int Transmission error interrupt.
 * @param[in] half_int Half transmission done interrupt.
 * @param[in] full_int Full transmission done interrupt.
 */
void dma_config(size_t base, uint8_t channel, void *mem, void *phr, uint16_t size, bool mem2mem, uint8_t level,
		uint8_t mem_sz, uint8_t phr_sz, bool mem_inc, bool phr_inc, bool circ, bool mem2phr, bool err_int,
		bool half_int, bool full_int);

/**
 * @brief Get memory address for the DMA channel.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @return Memory address.
 */
size_t base_get_mem_addr(size_t base, uint8_t channel);

/**
 * @brief Get peripheral address for the DMA channel.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @return Peripheral address.
 */
size_t base_get_phr_addr(size_t base, uint8_t channel);

/**
 * @brief Get DMA data count.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @return Number of data.
 */
uint16_t dma_get_count(size_t base, uint8_t channel);

/**
 * @brief Set DMA data count.
 * This is used for changing count on already configured DMA channel.
 * DMA needs to be disabled before configuration.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @param[in] count Number of bytes to transmit.
 */
void dma_set_count(size_t base, uint8_t channel, uint16_t count);

/**
 * @brief Set DMA channel memory address.
 * This is used for changing addresses on already configured DMA channel.
 * DMA needs to be disabled before configuration.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @param[in] address Memory address.
 */
void dma_set_mem_addr(size_t base, uint8_t channel, void *address);

/**
 * @brief Set DMA channel peripheral address.
 * This is used for changing addresses on already configured DMA channel.
 * DMA needs to be disabled before configuration.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @param[in] address Peripheral address.
 */
void dma_set_phr_address(size_t base, uint8_t channel, void *address);

/**
 * @brief Enable DMA channel.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 */
void dma_enable(size_t base, uint8_t channel);

/**
 * @brief Disable DMA channel.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 */
void dma_disable(size_t base, uint8_t channel);

/**
 * @brief Get DMA channel interrupt status.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @return Interrupt status.
 */
dma_interrupt_t dma_int_status(size_t base, uint8_t channel);

/**
 * @brief Clear selected DMA interrupts.
 *
 * @param[in] base DMA block.
 * @param[in] channel DMA channel.
 * @param[in] mask DMA bit mask.
 */
void dma_int_clear(size_t base, uint8_t channel, dma_interrupt_t mask);
