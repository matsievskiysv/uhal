#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/rcc.h"
#elif defined(set32f103h)
#include "../stm32f103h/rcc.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

#include <stdbool.h>
#include <stdint.h>

typedef struct {
	rcc_ahbenr_t  ahb;
	rcc_apb1enr_t apb1;
	rcc_apb2enr_t apb2;
} rcc_clock_t;

/**
 * @brief Start internal 8MHz clock.
 */
void rcc_start_internal_clock(void);

/**
 * @brief Start external clock.
 */
void rcc_start_external_clock(void);

/**
 * @brief Start PLL clock.
 *
 * @param[in] from_external Use external or internal PLL source.
 * @param[in] external_divide Divide external PLL source.
 * @param[in] pll_mul PLL multiplication factor.
 * @param[in] adb1_div Low speed bus clock divisor.
 * @param[in] adc_div ADC clock divisor.
 * @param[in] usb_div Enable USB clock divisor.
 * @param[in] latency Flash access latency.
 * @retval sum Special return value.
 * @return The sum of \p a and \p b.
 */
void rcc_start_pll_clock(bool from_external, bool external_divide, uint8_t pll_mul, uint8_t adb1_div, uint8_t adb2_div,
			 uint8_t ahb_div, uint8_t adc_div, bool usb_div, uint8_t latency);

/**
 * @brief Set ADC clock prescaler.
 *
 * @param[in] val Prescaler value.
 */
void rcc_adc_prescaler(size_t val);

/**
 * @brief Enable subsystem clock.
 *
 * @param[in] mask Clock mask.
 */
void rcc_clock_enable(rcc_clock_t mask);

/**
 * @brief Disable subsystem clock.
 *
 * @param[in] mask Clock mask.
 */
void rcc_clock_disable(rcc_clock_t mask);
