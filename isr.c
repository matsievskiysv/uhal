extern void _stack_start(void);

__attribute__((weak, interrupt)) void
reset_handler(void)
{
	for (;;) {}
}

__attribute__((weak, interrupt)) void
default_handler(void)
{
	for (;;) {}
}

__attribute__((weak, interrupt)) void
hard_fault_handler(void)
{
	for (;;) {}
}

void nmi_handler(void) __attribute__((weak, interrupt, alias("hard_fault_handler")));
void memmanage_handler(void) __attribute__((weak, interrupt, alias("hard_fault_handler")));
void busfault_handler(void) __attribute__((weak, interrupt, alias("hard_fault_handler")));
void usagefault_handler(void) __attribute__((weak, interrupt, alias("hard_fault_handler")));
void svc_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void debugmon_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void pendsv_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void systick_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void wwdg_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void pvd_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tamper_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void rtc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void flash_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void rcc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti0_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti4_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel4_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel5_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel6_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel7_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void adc1_2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usb_hp_can1_tx_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usb_lp_can1_rx0_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void can1_rx1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void can1_sce_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti9_5_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim1_brk_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim1_up_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim1_trg_com_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim1_cc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim4_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void i2c1_ev_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void i2c1_er_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void i2c2_ev_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void i2c2_er_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void spi1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void spi2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usart1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usart2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usart3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti15_10_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void rtc_alarm_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usbwakeup_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim8_brk_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim8_up_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim8_trg_com_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim8_cc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void adc3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void fsmc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void sdio_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim5_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void spi3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void uart4_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void uart5_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim6_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim7_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma2_channel1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma2_channel2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma2_channel3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma2_channel4_5_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(stm32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "stm32f103m/isr.c"
#elif defined(stm32f103h)
#include "stm32f103h/isr.c"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif
