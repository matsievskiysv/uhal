#pragma once

#include <stdbool.h>
#include <stddef.h>

#define EXTI 0x4001##0400##UL

typedef union {
	struct __attribute__((packed)) {
		bool mr0  : 1;
		bool mr1  : 1;
		bool mr2  : 1;
		bool mr3  : 1;
		bool mr4  : 1;
		bool mr5  : 1;
		bool mr6  : 1;
		bool mr7  : 1;
		bool mr8  : 1;
		bool mr9  : 1;
		bool mr10 : 1;
		bool mr11 : 1;
		bool mr12 : 1;
		bool mr13 : 1;
		bool mr14 : 1;
		bool mr15 : 1;
		bool mr16 : 1;
		bool mr17 : 1;
		bool mr18 : 1;
		bool mr19 : 1;
	} bits;
	size_t val;
} exti_imr_t;
_Static_assert(sizeof(exti_imr_t) == sizeof(size_t), "invalid exti_imr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool mr0  : 1;
		bool mr1  : 1;
		bool mr2  : 1;
		bool mr3  : 1;
		bool mr4  : 1;
		bool mr5  : 1;
		bool mr6  : 1;
		bool mr7  : 1;
		bool mr8  : 1;
		bool mr9  : 1;
		bool mr10 : 1;
		bool mr11 : 1;
		bool mr12 : 1;
		bool mr13 : 1;
		bool mr14 : 1;
		bool mr15 : 1;
		bool mr16 : 1;
		bool mr17 : 1;
		bool mr18 : 1;
		bool mr19 : 1;
	} bits;
	size_t val;
} exti_emr_t;
_Static_assert(sizeof(exti_emr_t) == sizeof(size_t), "invalid exti_emr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool tr0  : 1;
		bool tr1  : 1;
		bool tr2  : 1;
		bool tr3  : 1;
		bool tr4  : 1;
		bool tr5  : 1;
		bool tr6  : 1;
		bool tr7  : 1;
		bool tr8  : 1;
		bool tr9  : 1;
		bool tr10 : 1;
		bool tr11 : 1;
		bool tr12 : 1;
		bool tr13 : 1;
		bool tr14 : 1;
		bool tr15 : 1;
		bool tr16 : 1;
		bool tr17 : 1;
		bool tr18 : 1;
		bool tr19 : 1;
	} bits;
	size_t val;
} exti_rtsr_t;
_Static_assert(sizeof(exti_rtsr_t) == sizeof(size_t), "invalid exti_rtsr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool tr0  : 1;
		bool tr1  : 1;
		bool tr2  : 1;
		bool tr3  : 1;
		bool tr4  : 1;
		bool tr5  : 1;
		bool tr6  : 1;
		bool tr7  : 1;
		bool tr8  : 1;
		bool tr9  : 1;
		bool tr10 : 1;
		bool tr11 : 1;
		bool tr12 : 1;
		bool tr13 : 1;
		bool tr14 : 1;
		bool tr15 : 1;
		bool tr16 : 1;
		bool tr17 : 1;
		bool tr18 : 1;
		bool tr19 : 1;
	} bits;
	size_t val;
} exti_ftsr_t;
_Static_assert(sizeof(exti_ftsr_t) == sizeof(size_t), "invalid exti_ftsr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool pr0  : 1;
		bool pr1  : 1;
		bool pr2  : 1;
		bool pr3  : 1;
		bool pr4  : 1;
		bool pr5  : 1;
		bool pr6  : 1;
		bool pr7  : 1;
		bool pr8  : 1;
		bool pr9  : 1;
		bool pr10 : 1;
		bool pr11 : 1;
		bool pr12 : 1;
		bool pr13 : 1;
		bool pr14 : 1;
		bool pr15 : 1;
		bool pr16 : 1;
		bool pr17 : 1;
		bool pr18 : 1;
		bool pr19 : 1;
	} bits;
	size_t val;
} exti_pr_t;
_Static_assert(sizeof(exti_pr_t) == sizeof(size_t), "invalid exti_pr_t size");

typedef volatile struct {
	exti_imr_t  imr;
	exti_emr_t  emr;
	exti_rtsr_t rtsr;
	exti_ftsr_t ftsr;
	exti_pr_t   pr;
} exti_t;
_Static_assert(sizeof(exti_t) == 5 * sizeof(size_t), "invalid exti_t size");
