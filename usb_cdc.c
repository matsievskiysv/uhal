#include <usb_cdc.h>
#include <util.h>

void
usb_handle_cdc_setup(uint8_t ep)
{
	usb_ep_t  *usb_ep = GET_REG(USB_EP);
	usb_epnr_t ep_r	  = usb_ep->ep[ep];
	if (usb_cdc_global_state.setting_line &&
	    (usb_btable[ep].count_rx.bits.countn_rx == sizeof(usb_cdc_line_coding_t))) {
		usb_cdc_global_state.setting_line = false;
		memcpy_shared2ram(&usb_cdc_global_state.line_coding,
				  (void *) SHARED_SRAM_LOCAL_TO_GLOBAL(usb_btable[ep].addr_rx.bits.addrn_rx),
				  sizeof(usb_cdc_line_coding_t));
		usb_endpoint_transfer_zero_size(ep);
		usb_endpoint_start_tx(ep);
		return;
	} else if (ep_r.bits.setup && ep_r.bits.ctr_rx &&
		   (usb_btable[ep].count_rx.bits.countn_rx == sizeof(usb_setup_packet_t))) {
		usb_setup_packet_shared_t *packet =
		    (void *) SHARED_SRAM_LOCAL_TO_GLOBAL(usb_btable[ep].addr_rx.bits.addrn_rx);
		switch ((usb_cdc_request_t) packet->bRequest) {
		case USB_CDC_REQUEST_SEND_ENCAPSULATED_COMMAND: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CDC_REQUEST_GET_ENCAPSULATED_RESPONSE: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) {
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CDC_REQUEST_SET_LINE_CODING: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
			    (packet->wLength == sizeof(usb_cdc_line_coding_t))) {
				usb_cdc_global_state.setting_line = true;
				usb_endpoint_start_rx(ep);
				return;
			}
			break;
		}
		case USB_CDC_REQUEST_GET_LINE_CODING: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) {
				usb_endpoint_start_rx(ep);
				return;
			}
			break;
		}
		}
	}
	usb_endpoint_start_rx(ep);
	usb_endpoint_stall_tx(ep);
}
