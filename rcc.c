#include <flash.h>
#include <rcc.h>
#include <util.h>

void
rcc_start_internal_clock(void)
{
	rcc_t *rcc = GET_REG(RCC);
	// enable 8MHz oscillator
	rcc_cr_t cr = {.bits = {.hsion = true}};
	REG_SET(&rcc->cr.val, cr.val);
	// wait for 8MHz oscillator to stabilize
	WAIT_WHILE(!rcc->cr.bits.hsirdy);
	rcc_cfgr_t cfgr_val  = {.bits = {.sw = RCC_SW_HSI}};
	rcc_cfgr_t cfgr_mask = {.bits = {.sw = 0b11}};
	REG_WRITE_MASK(&rcc->cfgr.val, cfgr_val.val, cfgr_mask.val);
	WAIT_WHILE(rcc->cfgr.bits.sws != RCC_SWS_HSI);
}

void
rcc_start_external_clock(void)
{
	rcc_t	*rcc = GET_REG(RCC);
	rcc_cr_t cr  = {.bits = {.hseon = true, .csson = true}};
	REG_SET(&rcc->cr.val, cr.val);
	// wait for oscillator to stabilize
	WAIT_WHILE(!rcc->cr.bits.hserdy);
	rcc_cfgr_t cfgr_val  = {.bits = {.sw = RCC_SW_HSE}};
	rcc_cfgr_t cfgr_mask = {.bits = {.sw = 0b11}};
	REG_WRITE_MASK(&rcc->cfgr.val, cfgr_val.val, cfgr_mask.val);
	WAIT_WHILE(rcc->cfgr.bits.sws != RCC_SWS_HSE);
}

void
rcc_start_pll_clock(bool from_external, bool external_divide, uint8_t pll_mul, uint8_t adb1_div, uint8_t adb2_div,
		    uint8_t ahb_div, uint8_t adc_div, bool usb_div, uint8_t latency)
{
	rcc_t *rcc = GET_REG(RCC);
	if (from_external)
		rcc_start_external_clock();
	else
		rcc_start_internal_clock();
	{
		rcc_cfgr_t cfgr = {.bits = {.pllsrc = from_external, .pllxtpre = external_divide}};
		REG_SET(&rcc->cfgr.val, cfgr.val);
	}
	{
		rcc_cfgr_t cfgr_val  = {.bits = {.pllmul = pll_mul,
						 .ppre1	 = adb1_div,
						 .ppre2	 = adb2_div,
						 .hpre	 = ahb_div,
						 .adcpre = adc_div,
						 .usbpre = !usb_div}};
		rcc_cfgr_t cfgr_mask = {.bits = {.pllmul = RCC_PLLMUL_16X,
						 .ppre1	 = RCC_PPRE1_16,
						 .ppre2	 = RCC_PPRE2_8,
						 .hpre	 = RCC_HPRE_512,
						 .adcpre = RCC_ADCPRE_8,
						 .usbpre = true}};
		REG_WRITE_MASK(&rcc->cfgr.val, cfgr_val.val, cfgr_mask.val);
	}
	rcc_cr_t cr = {.bits = {.pllon = true, .csson = true}};
	REG_SET(&rcc->cr.val, cr.val);
	// wait for oscillator to stabilize
	WAIT_WHILE(!rcc->cr.bits.pllrdy);
	flash_latency(latency);
	rcc_cfgr_t cfgr_val  = {.bits = {.sw = RCC_SW_PLL}};
	rcc_cfgr_t cfgr_mask = {.bits = {.sw = 0b11}};
	REG_WRITE_MASK(&rcc->cfgr.val, cfgr_val.val, cfgr_mask.val);
	WAIT_WHILE(rcc->cfgr.bits.sws != RCC_SWS_PLL);
}

void
rcc_adc_prescaler(size_t val)
{
	rcc_t	  *rcc	     = GET_REG(RCC);
	rcc_cfgr_t cfgr_val  = {.bits = {.adcpre = val}};
	rcc_cfgr_t cfgr_mask = {.bits = {.adcpre = RCC_ADCPRE_8}};
	REG_WRITE_MASK(&rcc->cfgr.val, cfgr_val.val, cfgr_mask.val);
}

void
rcc_clock_enable(rcc_clock_t mask)
{
	rcc_t *rcc = GET_REG(RCC);
	REG_SET(&rcc->ahbenr.val, mask.ahb.val);
	REG_SET(&rcc->apb1enr.val, mask.apb1.val);
	REG_SET(&rcc->apb2enr.val, mask.apb2.val);
}

void
rcc_clock_disable(rcc_clock_t mask)
{
	rcc_t *rcc = GET_REG(RCC);
	REG_CLEAR(&rcc->ahbenr.val, mask.ahb.val);
	REG_CLEAR(&rcc->apb1enr.val, mask.apb1.val);
	REG_CLEAR(&rcc->apb2enr.val, mask.apb2.val);
}
