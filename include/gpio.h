#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/gpio.h"
#elif defined(set32f103h)
#include "../stm32f103h/gpio.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define GPIO_SPEED_LOW	GPIO_SPEED_2MHZ
#define GPIO_SPEED_MED	GPIO_SPEED_10MHZ
#define GPIO_SPEED_HIGH GPIO_SPEED_50MHZ

/**
 * @brief Configure GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @param[in] mode Electrical mode.
 * Possible values are GPIO_IN_ANALOG, GPIO_IN_FLOAT, GPIO_IN_PULL.
 * @param[in] pull_level true to pull voltage up, false to pull down. Significant only when \p mode is GPIO_IN_PULL.
 */
void gpio_configure_input(size_t bank, uint8_t pin_number, uint8_t mode, bool pull_level);

/**
 * @brief Configure GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @param[in] mode Electrical mode.
 * Possible values are GPIO_OUT_PULL, GPIO_OUT_OPEN, GPIO_ALT_PUSH, GPIO_ALT_OPEN.
 * @param[in] speed GPIO speed. Ignored for input direction.
 * Possible values are GPIO_SPEED_LOW, GPIO_SPEED_MED and GPIO_SPEED_MAX.
 */
void gpio_configure_output(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t speed);

/**
 * @brief Lock GPIO pin until next uC reset.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_mask GPIO pin mask.
 */
void gpio_lock(size_t bank, size_t pin_mask);

/**
 * @brief Read GPIO pins.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_mask GPIO pin.
 * @return GPIO pin input value.
 */
size_t gpio_read_pins(size_t bank, uint16_t pin_mask);

/**
 * @brief Read GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @return GPIO pin input value.
 */
bool gpio_read_pin(size_t bank, uint8_t pin_number);

/**
 * @brief Write GPIO pins.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_mask GPIO pin mask.
 * @param[in] value GPIO pins value.
 */
void gpio_write_pins(size_t bank, uint16_t pin_mask, bool value);

/**
 * @brief Write GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin mask.
 * @param[in] value GPIO pin value.
 */
void gpio_write_pin(size_t bank, uint8_t pin_number, bool value);

/**
 * @brief Toggle GPIO pins.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_mask GPIO pin mask.
 */
void gpio_toggle_pins(size_t bank, uint16_t pin_mask);

/**
 * @brief Toggle GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin number.
 */
void gpio_toggle_pin(size_t bank, uint8_t pin_number);
