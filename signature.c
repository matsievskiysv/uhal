#include <signature.h>
#include <util.h>

uint16_t
flash_size()
{
	f_size_t *sz = GET_REG(FLASH_SZ);
	return sz->bits.f_size;
}

unique_id_t *
unique_id(void)
{
	return GET_REG(UNIQUE_ID);
}
