#include <usb_cp210x.h>
#include <util.h>

void
usb_handle_cp210x_setup(uint8_t ep)
{
	usb_ep_t  *usb_ep = GET_REG(USB_EP);
	usb_epnr_t ep_r	  = usb_ep->ep[ep];
	if (usb_cp210x_global_state.setting_in_progress) {
		if (usb_cp210x_global_state.setting_baudrate &&
		    (usb_btable[ep].count_rx.bits.countn_rx == sizeof(usb_cp210x_global_state.baudrate))) {
			memcpy_shared2ram(&usb_cp210x_global_state.baudrate, usb_global_state.endpoints[0].rx_buffer,
					  sizeof(uint32_t));
			usb_endpoint_transfer_zero_size(ep);
			usb_endpoint_start_tx(ep);
			usb_cp210x_global_state.setting_baudrate = false;
			return;
		} else if (usb_cp210x_global_state.setting_control_chars &&
			   (usb_btable[ep].count_rx.bits.countn_rx == sizeof(usb_cp210x_global_state.control_chars))) {
			memcpy_shared2ram(usb_cp210x_global_state.control_chars,
					  usb_global_state.endpoints[0].rx_buffer,
					  sizeof(usb_cp210x_global_state.control_chars));
			usb_endpoint_transfer_zero_size(ep);
			usb_endpoint_start_tx(ep);
			usb_cp210x_global_state.setting_control_chars = false;
			return;
		} else if (usb_cp210x_global_state.setting_flow &&
			   (usb_btable[ep].count_rx.bits.countn_rx == sizeof(usb_cp210x_global_state.flow_control))) {
			memcpy_shared2ram(&usb_cp210x_global_state.flow_control,
					  usb_global_state.endpoints[0].rx_buffer,
					  sizeof(usb_cp210x_global_state.flow_control));
			BKPT;
			usb_endpoint_transfer_zero_size(ep);
			usb_endpoint_start_tx(ep);
			usb_cp210x_global_state.setting_flow = false;
			return;
		}
	} else if (ep_r.bits.setup && ep_r.bits.ctr_rx &&
		   (usb_btable[ep].count_rx.bits.countn_rx == sizeof(usb_setup_packet_t))) {
		usb_cp210x_global_state.setting_in_progress = 0;
		usb_setup_packet_shared_t *packet =
		    (void *) SHARED_SRAM_LOCAL_TO_GLOBAL(usb_btable[ep].addr_rx.bits.addrn_rx);
		// if (packet->bRequest == USB_CP210X_REQUEST_SET_BAUDRATE)
		// 	BKPT;
		switch ((usb_cp210x_request_t) packet->bRequest) {
		case USB_CP210X_REQUEST_IFC_ENABLE: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				for (size_t ep_idx = 1; ep_idx < usb_global_state.endpoint_count; ep_idx++) {
					usb_global_state.endpoints[ep_idx].halt = !packet->wValue;
				}
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_BAUDRATE: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.baudrate))) {
				usb_cp210x_global_state.setting_baudrate = true;
				usb_endpoint_start_rx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_BAUDRATE: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.baudrate))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.baudrate,
							     sizeof(usb_cp210x_global_state.baudrate));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_LINE_CTL: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				usb_cp210x_global_state.line_ctl.val = packet->wValue;
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_LINE_CTL: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.line_ctl.val,
							     sizeof(usb_cp210x_global_state.line_ctl));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_CHARS: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.control_chars))) {
				usb_cp210x_global_state.setting_control_chars = true;
				usb_endpoint_start_rx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_CHARS: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.control_chars))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.control_chars,
							     sizeof(usb_cp210x_global_state.control_chars));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_FLOW: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.flow_control))) {
				usb_cp210x_global_state.setting_flow = false;
				usb_endpoint_start_rx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_FLOW: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.flow_control))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.flow_control,
							     sizeof(usb_cp210x_global_state.flow_control));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_MHS: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				usb_cp210x_global_state.mhs.val = packet->wValue;
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_COMM_STATUS: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.serial_status))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.serial_status,
							     sizeof(usb_cp210x_global_state.serial_status));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_MDMSTS: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.mdmsts))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.mdmsts,
							     sizeof(usb_cp210x_global_state.mdmsts));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_XON: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				// FIXME: handle xon //
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_XOFF: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				// FIXME: handle xoff //
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_EVENTMASK: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				usb_cp210x_global_state.event.mask.val = packet->wValue;
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_EVENTMASK: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.event.mask))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.event.mask,
							     sizeof(usb_cp210x_global_state.event.mask));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_EVENTSTATE: {
			if ((packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) &&
			    (packet->wLength == sizeof(usb_cp210x_global_state.event.state))) {
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.event.state,
							     sizeof(usb_cp210x_global_state.event.state));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_CHAR: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_OUT) {
				uint8_t char_idx = MIN(packet->wValue & 0xf, (USB_CP210X_CONTROL_CHARS_MAX - 1));
				char	char_val = packet->wValue >> 4;
				usb_cp210x_global_state.control_chars[char_idx] = char_val;
				usb_endpoint_transfer_zero_size(ep);
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_GET_PROPS: {
			if (packet->DataTransferDirection == USB_ENDPOINT_DIRECTION_IN) {
				usb_cp210x_global_state.props.wLength = sizeof(usb_cp210x_global_state.props);
				usb_endpoint_transfer_single(ep, &usb_cp210x_global_state.props,
							     sizeof(usb_cp210x_global_state.props));
				usb_endpoint_start_tx(ep);
				return;
			}
			break;
		}
		case USB_CP210X_REQUEST_VENDOR_SPECIFIC: {
			switch ((usb_cp210x_vendor_specific_request_t) packet->wValue) {
			case USB_CP210X_VENDOR_SPECIFIC_REQUEST_GET_PARTNUM: {
				uint8_t buffer[1] = {USB_CP210X_CP2101_PARTNUM};
				usb_endpoint_transfer_single(ep, buffer, sizeof(buffer));
				usb_endpoint_start_tx(ep);
				return;
			}
			default:
				break;
			}
			break;
		}
		case USB_CP210X_REQUEST_SET_BREAK: // FIXME: handle break //
		case USB_CP210X_REQUEST_EMBED_EVENTS:
		case USB_CP210X_REQUEST_PURGE:
		case USB_CP210X_REQUEST_RESET: {
			usb_endpoint_transfer_zero_size(ep);
			usb_endpoint_start_tx(ep);
			return;
		}
		case USB_CP210X_REQUEST_SET_RECEIVE:
		case USB_CP210X_REQUEST_GET_RECEIVE:
		case USB_CP210X_REQUEST_IMM_CHAR:
		case USB_CP210X_REQUEST_SET_BAUDDIV:
		case USB_CP210X_REQUEST_GET_BAUDDIV: {
			break;
		}
		}
	}
	usb_endpoint_start_rx(ep);
	usb_endpoint_stall_tx(ep);
}
