#pragma once

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief USB full speed device endpoint zero packet size.
 */
#define USB_EP0_SIZE 64

/**
 * @brief Convert current to USB standard 2mA units.
 */
#define USB_POWER_MA(x) (x / 2)

/**
 * @brief Implemented USB revision.
 */
#define USB_REVISION	0x0200

/**
 * @brief USB setup packet direction.
 */
typedef enum {
	USB_SETUP_DIR_OUT = false,
	USB_SETUP_DIR_IN  = true,
} usb_setup_dir_t;

/**
 * @brief USB setup packet type.
 */
typedef enum {
	USB_SETUP_TYPE_STANDARD = 0,
	USB_SETUP_TYPE_CLASS	= 1,
	USB_SETUP_TYPE_VENDOR	= 2,
	USB_SETUP_TYPE_RESERVED = 3,
} usb_setup_type_t;

/**
 * @brief USB setup packet recipient.
 */
typedef enum {
	USB_SETUP_RECIPIENT_DEVICE    = 0,
	USB_SETUP_RECIPIENT_INTERFACE = 1,
	USB_SETUP_RECIPIENT_ENDPOINT  = 2,
	USB_SETUP_RECIPIENT_OTHER     = 3,
} usb_setup_recipient_t;

/**
 * @brief USB setup packet.
 */
typedef struct __attribute__((packed)) {
	union {
		uint8_t bmRequestType;
		struct {
			uint8_t Recipient	      : 5;
			uint8_t Type		      : 2;
			bool	DataTransferDirection : 1;
		};
	};
	uint8_t bRequest;
	union {
		uint16_t wValue;
		struct {
			uint8_t DescriptorIndex;
			uint8_t DescriptorType;
		};
	};
	uint16_t wIndex;
	uint16_t wLength;
} usb_setup_packet_t;
_Static_assert(sizeof(usb_setup_packet_t) == sizeof(uint8_t) * 8, "invalid usb_setup_packet_t size");

/**
 * @brief USB setup packet in shared RAM view from application.
 */
typedef struct __attribute__((packed)) {
	union {
		uint8_t bmRequestType;
		struct {
			uint8_t Recipient	      : 5;
			uint8_t Type		      : 2;
			bool	DataTransferDirection : 1;
		};
	};
	uint8_t bRequest;
	uint16_t : 16;
	union {
		uint16_t wValue;
		struct {
			uint8_t DescriptorIndex;
			uint8_t DescriptorType;
		};
	};
	uint16_t : 16;
	uint16_t wIndex;
	uint16_t : 16;
	uint16_t wLength;
	uint16_t : 16;
} usb_setup_packet_shared_t;
_Static_assert(sizeof(usb_setup_packet_shared_t) == sizeof(usb_setup_packet_t) * 2,
	       "invalid usb_setup_packet_shared_t size");

/**
 * @brief USB standard setup requests.
 */
typedef enum {
	USB_REQUEST_GET_STATUS	      = 0,
	USB_REQUEST_CLEAR_FEATURE     = 1,
	USB_REQUEST_SET_FEATURE	      = 3,
	USB_REQUEST_SET_ADDRESS	      = 5,
	USB_REQUEST_GET_DESCRIPTOR    = 6,
	USB_REQUEST_SET_DESCRIPTOR    = 7,
	USB_REQUEST_GET_CONFIGURATION = 8,
	USB_REQUEST_SET_CONFIGURATION = 9,
	USB_REQUEST_GET_INTERFACE     = 10,
	USB_REQUEST_SET_INTERFACE     = 11,
	USB_REQUEST_SYNCH_FRAME	      = 12
} usb_request_t;

/**
 * @brief USB standard descriptors.
 */
typedef enum {
	USB_DESCRIPTOR_DEVICE			 = 1,
	USB_DESCRIPTOR_CONFIGURATION		 = 2,
	USB_DESCRIPTOR_STRING			 = 3,
	USB_DESCRIPTOR_INTERFACE		 = 4,
	USB_DESCRIPTOR_ENDPOINT			 = 5,
	USB_DESCRIPTOR_DEVICE_QUALIFIER		 = 6,
	USB_DESCRIPTOR_OTHER_SPEED_CONFIGURATION = 7,
	USB_DESCRIPTOR_INTERFACE_POWER		 = 8,
} usb_descriptor_t;

/**
 * @brief USB endpoint transfer type.
 */
typedef enum {
	USB_TRANSFER_TYPE_CONTROL     = 0b00,
	USB_TRANSFER_TYPE_ISOCHRONOUS = 0b01,
	USB_TRANSFER_TYPE_BULK	      = 0b10,
	USB_TRANSFER_TYPE_INTERRUPT   = 0b11,
} usb_transfer_type_t;

/**
 * @brief USB endpoint synchronization.
 */
typedef enum {
	USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION = 0b00,
	USB_SYNCHRONIZATION_TYPE_ASYNCHRONOUS	    = 0b01,
	USB_SYNCHRONIZATION_TYPE_ADAPTIVE	    = 0b10,
	USB_SYNCHRONIZATION_TYPE_SYNCHRONOUS	    = 0b11,
} usb_synchronization_type_t;

/**
 * @brief USB endpoint usage type.
 */
typedef enum {
	USB_USAGE_TYPE_DATA_ENDPOINT		       = 0b00,
	USB_USAGE_TYPE_FEEDBACK_ENDPOINT	       = 0b01,
	USB_USAGE_TYPE_IMPLICIT_FEEDBACK_DATA_ENDPOINT = 0b10,
	USB_USAGE_TYPE_RESERVED			       = 0b11,
} usb_usage_type_t;

/**
 * @brief USB endpoint direction.
 */
typedef enum {
	USB_ENDPOINT_DIRECTION_OUT = 0,
	USB_ENDPOINT_DIRECTION_IN  = 1,
} usb_endpoint_direction_t;

/**
 * @brief USB standard features.
 */
typedef enum {
	USB_FEATURE_ENDPOINT_HALT	 = 0,
	USB_FEATURE_DEVICE_REMOTE_WAKEUP = 1,
	USB_FEATURE_TEST_MODE		 = 2,
} usb_feature_t;

/**
 * @brief USB device descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t	 bLength;
	uint8_t	 bDescriptorType;
	uint16_t bcdUSB;
	uint8_t	 bDeviceClass;
	uint8_t	 bDeviceSubClass;
	uint8_t	 bDeviceProtocol;
	uint8_t	 bMaxPacketSize0;
	uint16_t idVendor;
	uint16_t idProduct;
	uint16_t bcdDevice;
	uint8_t	 iManufacturer;
	uint8_t	 iProduct;
	uint8_t	 iSerialNumber;
	uint8_t	 bNumConfigurations;
} usb_descriptor_device_t;
_Static_assert(sizeof(usb_descriptor_device_t) == sizeof(uint8_t) * 18, "invalid usb_descriptor_device_t size");

/**
 * @brief USB device descriptor instance.
 */
extern const usb_descriptor_device_t device_descriptor;

/**
 * @brief USB configuration descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t	 bLength;
	uint8_t	 bDescriptorType;
	uint16_t wTotalLength;
	uint8_t	 bNumInterfaces;
	uint8_t	 bConfigurationValue;
	uint8_t	 iConfiguration;
	union {
		uint8_t bmAttributes;
		struct {
			uint8_t		  : 5;
			bool RemoteWakeup : 1;
			bool SelfPowered  : 1;
			bool ReservedAttr : 1; ///< Reserved field. Must be set to 1.
		};
	};
	uint8_t bMaxPower;
} usb_descriptor_config_t;
_Static_assert(sizeof(usb_descriptor_config_t) == sizeof(uint8_t) * 9, "invalid usb_descriptor_config_t size");

/**
 * @brief USB interface descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bInterfaceNumber;
	uint8_t bAlternateSetting;
	uint8_t bNumEndpoints;
	uint8_t bInterfaceClass;
	uint8_t bInterfaceSubClass;
	uint8_t bInterfaceProtocol;
	uint8_t iInterface;
} usb_descriptor_interface_t;
_Static_assert(sizeof(usb_descriptor_interface_t) == sizeof(uint8_t) * 9, "invalid usb_descriptor_interface_t size");

/**
 * @brief USB endpoint descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t bLength;
	uint8_t bDescriptorType;
	union {
		uint8_t bEndpointAddress;
		struct {
			uint8_t EndpointNumber : 4;
			uint8_t		       : 3;
			bool Direction	       : 1;
		};
	};
	union {
		uint8_t bmAttributes;
		struct {
			uint8_t TransferType	    : 2;
			uint8_t SynchronizationType : 2;
			uint8_t UsageType	    : 2;
		};
	};
	uint16_t wMaxPacketSize;
	uint8_t	 bInterval;
} usb_descriptor_endpoint_t;
_Static_assert(sizeof(usb_descriptor_endpoint_t) == sizeof(uint8_t) * 7, "invalid usb_descriptor_endpoint_t size");

/**
 * @brief USB language definitions.
 */
typedef enum {
	USB_DESCRIPTOR_LANGUAGE_ID_AFRIKAANS		      = 0x0436,
	USB_DESCRIPTOR_LANGUAGE_ID_ALBANIAN		      = 0x041C,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_SAUDI_ARABIA	      = 0x0401,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_IRAQ		      = 0x0801,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_EGYPT		      = 0x0C01,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_LIBYA		      = 0x1001,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_ALGERIA	      = 0x1401,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_MOROCCO	      = 0x1801,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_TUNISIA	      = 0x1C01,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_OMAN		      = 0x2001,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_YEMEN		      = 0x2401,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_SYRIA		      = 0x2801,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_JORDAN	      = 0x2C01,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_LEBANON	      = 0x3001,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_KUWAIT	      = 0x3401,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_UAE		      = 0x3801,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_BAHRAIN	      = 0x3C01,
	USB_DESCRIPTOR_LANGUAGE_ID_ARABIC_QATAR		      = 0x4001,
	USB_DESCRIPTOR_LANGUAGE_ID_ARMENIAN		      = 0x042B,
	USB_DESCRIPTOR_LANGUAGE_ID_ASSAMESE		      = 0x044D,
	USB_DESCRIPTOR_LANGUAGE_ID_AZERI_LATIN		      = 0x042C,
	USB_DESCRIPTOR_LANGUAGE_ID_AZERI_CYRILLIC	      = 0x082C,
	USB_DESCRIPTOR_LANGUAGE_ID_BASQUE		      = 0x042D,
	USB_DESCRIPTOR_LANGUAGE_ID_BELARUSSIAN		      = 0x0423,
	USB_DESCRIPTOR_LANGUAGE_ID_BENGALI		      = 0x0445,
	USB_DESCRIPTOR_LANGUAGE_ID_BULGARIAN		      = 0x0402,
	USB_DESCRIPTOR_LANGUAGE_ID_BURMESE		      = 0x0455,
	USB_DESCRIPTOR_LANGUAGE_ID_CATALAN		      = 0x0403,
	USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_TAIWAN	      = 0x0404,
	USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_PRC		      = 0x0804,
	USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_HONG_KONG_SAR      = 0x0C04,
	USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_HONG_KONG_PRC      = USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_HONG_KONG_SAR,
	USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_SINGAPORE	      = 0x1004,
	USB_DESCRIPTOR_LANGUAGE_ID_CHINESE_MACAU_SAR	      = 0x1404,
	USB_DESCRIPTOR_LANGUAGE_ID_CROATIAN		      = 0x041A,
	USB_DESCRIPTOR_LANGUAGE_ID_CZECH		      = 0x0405,
	USB_DESCRIPTOR_LANGUAGE_ID_DANISH		      = 0x0406,
	USB_DESCRIPTOR_LANGUAGE_ID_DUTCH_NETHERLANDS	      = 0x0413,
	USB_DESCRIPTOR_LANGUAGE_ID_DUTCH_BELGIUM	      = 0x0813,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_UNITED_STATES      = 0x0409,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_UNITED_KINGDOM     = 0x0809,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_AUSTRALIAN	      = 0x0C09,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_CANADIAN	      = 0x1009,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_NEW_ZEALAND	      = 0x1409,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_IRELAND	      = 0x1809,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_SOUTH_AFRICA	      = 0x1C09,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_JAMAICA	      = 0x2009,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_CARIBBEAN	      = 0x2409,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_BELIZE	      = 0x2809,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_TRINIDAD	      = 0x2C09,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_ZIMBABWE	      = 0x3009,
	USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_PHILIPPINES	      = 0x3409,
	USB_DESCRIPTOR_LANGUAGE_ID_ESTONIAN		      = 0x0425,
	USB_DESCRIPTOR_LANGUAGE_ID_FAEROESE		      = 0x0438,
	USB_DESCRIPTOR_LANGUAGE_ID_FARSI		      = 0x0429,
	USB_DESCRIPTOR_LANGUAGE_ID_FINNISH		      = 0x040B,
	USB_DESCRIPTOR_LANGUAGE_ID_FRENCH_STANDARD	      = 0x040C,
	USB_DESCRIPTOR_LANGUAGE_ID_FRENCH_BELGIAN	      = 0x080C,
	USB_DESCRIPTOR_LANGUAGE_ID_FRENCH_CANADIAN	      = 0x0C0C,
	USB_DESCRIPTOR_LANGUAGE_ID_FRENCH_SWITZERLAND	      = 0x100C,
	USB_DESCRIPTOR_LANGUAGE_ID_FRENCH_LUXEMBOURG	      = 0x140C,
	USB_DESCRIPTOR_LANGUAGE_ID_FRENCH_MONACO	      = 0x180C,
	USB_DESCRIPTOR_LANGUAGE_ID_GEORGIAN		      = 0x0437,
	USB_DESCRIPTOR_LANGUAGE_ID_GERMAN_STANDARD	      = 0x0407,
	USB_DESCRIPTOR_LANGUAGE_ID_GERMAN_SWITZERLAND	      = 0x0807,
	USB_DESCRIPTOR_LANGUAGE_ID_GERMAN_AUSTRIA	      = 0x0C07,
	USB_DESCRIPTOR_LANGUAGE_ID_GERMAN_LUXEMBOURG	      = 0x1007,
	USB_DESCRIPTOR_LANGUAGE_ID_GERMAN_LIECHTENSTEIN	      = 0x1407,
	USB_DESCRIPTOR_LANGUAGE_ID_GREEK		      = 0x0408,
	USB_DESCRIPTOR_LANGUAGE_ID_GUJARATI		      = 0x0447,
	USB_DESCRIPTOR_LANGUAGE_ID_HEBREW		      = 0x040D,
	USB_DESCRIPTOR_LANGUAGE_ID_HINDI		      = 0x0439,
	USB_DESCRIPTOR_LANGUAGE_ID_HUNGARIAN		      = 0x040E,
	USB_DESCRIPTOR_LANGUAGE_ID_ICELANDIC		      = 0x040F,
	USB_DESCRIPTOR_LANGUAGE_ID_INDONESIAN		      = 0x0421,
	USB_DESCRIPTOR_LANGUAGE_ID_ITALIAN_STANDARD	      = 0x0410,
	USB_DESCRIPTOR_LANGUAGE_ID_ITALIAN_SWITZERLAND	      = 0x0810,
	USB_DESCRIPTOR_LANGUAGE_ID_JAPANESE		      = 0x0411,
	USB_DESCRIPTOR_LANGUAGE_ID_KANNADA		      = 0x044B,
	USB_DESCRIPTOR_LANGUAGE_ID_KASHMIRI_INDIA	      = 0x0860,
	USB_DESCRIPTOR_LANGUAGE_ID_KAZAKH		      = 0x043F,
	USB_DESCRIPTOR_LANGUAGE_ID_KONKANI		      = 0x0457,
	USB_DESCRIPTOR_LANGUAGE_ID_KOREAN		      = 0x0412,
	USB_DESCRIPTOR_LANGUAGE_ID_KOREAN_JOHAB		      = 0x0812,
	USB_DESCRIPTOR_LANGUAGE_ID_LATVIAN		      = 0x0426,
	USB_DESCRIPTOR_LANGUAGE_ID_LITHUANIAN		      = 0x0427,
	USB_DESCRIPTOR_LANGUAGE_ID_LITHUANIAN_CLASSIC	      = 0x0827,
	USB_DESCRIPTOR_LANGUAGE_ID_MACEDONIAN		      = 0x042F,
	USB_DESCRIPTOR_LANGUAGE_ID_MALAY_MALAYSIAN	      = 0x043E,
	USB_DESCRIPTOR_LANGUAGE_ID_MALAY_BRUNEI_DARUSSALAM    = 0x083E,
	USB_DESCRIPTOR_LANGUAGE_ID_MALAYALAM		      = 0x044C,
	USB_DESCRIPTOR_LANGUAGE_ID_MANIPURI		      = 0x0458,
	USB_DESCRIPTOR_LANGUAGE_ID_MARATHI		      = 0x044E,
	USB_DESCRIPTOR_LANGUAGE_ID_NEPALI_INDIA		      = 0x0861,
	USB_DESCRIPTOR_LANGUAGE_ID_NORWEGIAN_BOKMAL	      = 0x0414,
	USB_DESCRIPTOR_LANGUAGE_ID_NORWEGIAN_NYNORSK	      = 0x0814,
	USB_DESCRIPTOR_LANGUAGE_ID_ORIYA		      = 0x0448,
	USB_DESCRIPTOR_LANGUAGE_ID_POLISH		      = 0x0415,
	USB_DESCRIPTOR_LANGUAGE_ID_PORTUGUESE_BRAZIL	      = 0x0416,
	USB_DESCRIPTOR_LANGUAGE_ID_PORTUGUESE_STANDARD	      = 0x0816,
	USB_DESCRIPTOR_LANGUAGE_ID_PUNJABI		      = 0x0446,
	USB_DESCRIPTOR_LANGUAGE_ID_ROMANIAN		      = 0x0418,
	USB_DESCRIPTOR_LANGUAGE_ID_RUSSIAN		      = 0x0419,
	USB_DESCRIPTOR_LANGUAGE_ID_SANSKRIT		      = 0x044F,
	USB_DESCRIPTOR_LANGUAGE_ID_SERBIAN_CYRILLIC	      = 0x0C1A,
	USB_DESCRIPTOR_LANGUAGE_ID_SERBIAN_LATIN	      = 0x081A,
	USB_DESCRIPTOR_LANGUAGE_ID_SINDHI		      = 0x0459,
	USB_DESCRIPTOR_LANGUAGE_ID_SLOVAK		      = 0x041B,
	USB_DESCRIPTOR_LANGUAGE_ID_SLOVENIAN		      = 0x0424,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_TRADITIONAL_SORT   = 0x040A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_MEXICAN	      = 0x080A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_MODERN_SORT	      = 0x0C0A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_GUATEMALA	      = 0x100A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_COSTA_RICA	      = 0x140A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_PANAMA	      = 0x180A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_DOMINICAN_REPUBLIC = 0x1C0A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_VENEZUELA	      = 0x200A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_COLOMBIA	      = 0x240A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_PERU		      = 0x280A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_ARGENTINA	      = 0x2C0A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_ECUADOR	      = 0x300A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_CHILE	      = 0x340A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_URUGUAY	      = 0x380A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_PARAGUAY	      = 0x3C0A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_BOLIVIA	      = 0x400A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_EL_SALVADOR	      = 0x440A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_HONDURAS	      = 0x480A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_NICARAGUA	      = 0x4C0A,
	USB_DESCRIPTOR_LANGUAGE_ID_SPANISH_PUERTO_RICO	      = 0x500A,
	USB_DESCRIPTOR_LANGUAGE_ID_SUTU			      = 0x0430,
	USB_DESCRIPTOR_LANGUAGE_ID_SWAHILI_KENYA	      = 0x0441,
	USB_DESCRIPTOR_LANGUAGE_ID_SWEDISH		      = 0x041D,
	USB_DESCRIPTOR_LANGUAGE_ID_SWEDISH_FINLAND	      = 0x081D,
	USB_DESCRIPTOR_LANGUAGE_ID_TAMIL		      = 0x0449,
	USB_DESCRIPTOR_LANGUAGE_ID_TATAR_TATARSTAN	      = 0x0444,
	USB_DESCRIPTOR_LANGUAGE_ID_TELUGU		      = 0x044A,
	USB_DESCRIPTOR_LANGUAGE_ID_THAI			      = 0x041E,
	USB_DESCRIPTOR_LANGUAGE_ID_TURKISH		      = 0x041F,
	USB_DESCRIPTOR_LANGUAGE_ID_UKRAINIAN		      = 0x0422,
	USB_DESCRIPTOR_LANGUAGE_ID_URDU_PAKISTAN	      = 0x0420,
	USB_DESCRIPTOR_LANGUAGE_ID_URDU_INDIA		      = 0x0820,
	USB_DESCRIPTOR_LANGUAGE_ID_UZBEK_LATIN		      = 0x0443,
	USB_DESCRIPTOR_LANGUAGE_ID_UZBEK_CYRILLIC	      = 0x0843,
	USB_DESCRIPTOR_LANGUAGE_ID_VIETNAMESE		      = 0x042A,
	USB_DESCRIPTOR_LANGUAGE_ID_HID_USAGE_DATA_DESCRIPTOR  = 0x04FF,
	USB_DESCRIPTOR_LANGUAGE_ID_HID_VENDOR_DEFINED_1	      = 0xF0FF,
	USB_DESCRIPTOR_LANGUAGE_ID_HID_VENDOR_DEFINED_2	      = 0xF4FF,
	USB_DESCRIPTOR_LANGUAGE_ID_HID_VENDOR_DEFINED_3	      = 0xF8FF,
	USB_DESCRIPTOR_LANGUAGE_ID_HID_VENDOR_DEFINED_4	      = 0xFCFF,
} usb_descriptor_language_id_t;

/**
 * @brief USB supported language descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t	 bLength;
	uint8_t	 bDescriptorType;
	uint16_t wLangID[];
} usb_descriptor_languages_t;

/**
 * @brief USB supported language descriptor instance.
 */
extern const usb_descriptor_languages_t usb_descriptor_languages;

/**
 * @brief USB string descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t	 bLength;
	uint8_t	 bDescriptorType;
	uint16_t bString[];
} usb_descriptor_string_t;
