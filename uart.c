#include <usart.h>
#include <util.h>

#ifdef __FPU_PRESENT
#include <math.h>
#endif

void
usart_baudrate(size_t port, int freq, int baud)
{
#ifdef __FPU_PRESENT
	float  fraction_real = freq / 16.0 / baud;
	size_t mantissa	     = floorf(fraction_real);
	size_t fraction	     = floorf((fraction_real - mantissa) * (USART_BAUD_FRACTION_MASK + 1));
#else
	size_t mantissa	     = freq / 16 / baud;
	size_t fraction_part = freq - mantissa * 16 * baud;
	size_t fraction	     = fraction_part / 16 * (USART_BAUD_FRACTION_MASK + 1) / baud;
#endif
	usart_t	   *usart = GET_REG(port);
	usart_bbr_t bbr	  = {.bits = {.mantissa = mantissa, .fraction = fraction}};
	REG_WRITE(&usart->bbr.val, bbr.val);
}

void
usart_configure(size_t port, bool nine_bit, bool parity, bool even_parity, bool send_break, size_t stop_bits)
{
	usart_t	   *usart    = GET_REG(port);
	usart_cr1_t cr1_val  = {.bits = {.m = nine_bit, .pce = parity, .ps = even_parity, .sbk = send_break}};
	usart_cr1_t cr1_mask = {.bits = {.m = true, .pce = true, .ps = true, .sbk = true}};
	REG_WRITE_MASK(&usart->cr1.val, cr1_val.val, cr1_mask.val);
	usart_cr2_t cr2_val  = {.bits = {.stop = stop_bits}};
	usart_cr2_t cr2_mask = {.bits = {.stop = USART_STOP_1_5}};
	REG_WRITE_MASK(&usart->cr2.val, cr2_val.val, cr2_mask.val);
}

uint8_t
usart_read(size_t port)
{
	usart_t *usart = GET_REG(port);
	return usart->dr.bits.dr;
}

uint8_t
usart_wait_read(size_t port)
{
	usart_t *usart = GET_REG(port);
	WAIT_WHILE(!usart->sr.bits.rxne);
	return usart->dr.bits.dr;
}

void
usart_write(size_t port, uint8_t data)
{
	usart_t	  *usart = GET_REG(port);
	usart_dr_t dr	 = {.bits = {.dr = data}};
	REG_WRITE(&usart->dr.val, dr.val);
}

usart_status_t
usart_status(size_t port)
{
	usart_t	      *usart  = GET_REG(port);
	usart_status_t status = {.val = usart->sr.val};
	return status;
}

void
usart_status_clear(size_t port, usart_status_t status)
{
	usart_t *usart = GET_REG(port);
	REG_CLEAR(&usart->sr.val, status.val);
}

void
usart_enable(size_t port)
{
	usart_t	   *usart = GET_REG(port);
	usart_cr1_t cr1	  = {.bits = {.re = true, .te = true, .ue = true}};
	REG_SET(&usart->cr1.val, cr1.val);
}

void
usart_disable(size_t port)
{
	usart_t	   *usart = GET_REG(port);
	usart_cr1_t cr1	  = {.bits = {.re = true, .te = true, .ue = true}};
	REG_CLEAR(&usart->cr1.val, cr1.val);
}

void
usart_configure_interrupt(size_t port, usart_status_t mask)
{
	usart_t	   *usart    = GET_REG(port);
	usart_cr1_t cr1_val  = {.bits = {
				    .peie   = mask.bits.parity_error,
				    .txeie  = mask.bits.transmit_buffer_empty,
				    .tcie   = mask.bits.transmission_complete,
				    .rxneie = mask.bits.recieve_buffer_not_empty,
				    .idleie = mask.bits.idle_detect,
				}};
	usart_cr1_t cr1_mask = {.bits = {
				    .peie   = true,
				    .txeie  = true,
				    .tcie   = true,
				    .rxneie = true,
				    .idleie = true,
				}};
	REG_WRITE_MASK(&usart->cr1.val, cr1_val.val, cr1_mask.val);
	usart_cr2_t cr2_val  = {.bits = {.lbdie = mask.bits.lin_detect}};
	usart_cr2_t cr2_mask = {.bits = {.lbdie = true}};
	REG_WRITE_MASK(&usart->cr2.val, cr2_val.val, cr2_mask.val);
	usart_cr3_t cr3_val = {
	    .bits = {.eie = mask.bits.framing_error | mask.bits.noize_error | mask.bits.overrun_error}};
	usart_cr3_t cr3_mask = {.bits = {.eie = true}};
	REG_WRITE_MASK(&usart->cr3.val, cr3_val.val, cr3_mask.val);
}

void
usart_dma(size_t port, bool dma_tx, bool dma_rx)
{
	usart_t	   *usart    = GET_REG(port);
	usart_cr3_t cr3_val  = {.bits = {.dmat = dma_tx, .dmar = dma_rx}};
	usart_cr3_t cr3_mask = {.bits = {.dmat = true, .dmar = true}};
	REG_WRITE_MASK(&usart->cr3.val, cr3_val.val, cr3_mask.val);
}
