#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define SYSTICK 0xE000##E010##UL

typedef union {
	struct __attribute__((packed)) {
		bool enable    : 1;
		bool tickint   : 1;
		bool clksource : 1;
		uint16_t       : 13;
		bool countflag : 1;
	} bits;
	size_t val;
} systick_csr_t;
_Static_assert(sizeof(systick_csr_t) == sizeof(size_t), "invalid systick_csr_t size");

typedef union {
	struct __attribute__((packed)) {
		size_t reload : 24;
	} bits;
	size_t val;
} systick_rvr_t;
_Static_assert(sizeof(systick_rvr_t) == sizeof(size_t), "invalid systick_rvr_t size");

typedef union {
	struct __attribute__((packed)) {
		size_t current : 24;
	} bits;
	size_t val;
} systick_cvr_t;
_Static_assert(sizeof(systick_cvr_t) == sizeof(size_t), "invalid systick_cvr_t size");

typedef union {
	struct __attribute__((packed)) {
		size_t tenms : 24;
		uint8_t	     : 6;
		bool skew    : 1;
		bool noref   : 1;
	} bits;
	size_t val;
} systick_calib_t;
_Static_assert(sizeof(systick_calib_t) == sizeof(size_t), "invalid systick_calib_t size");

typedef volatile struct {
	systick_csr_t	csr;
	systick_rvr_t	rvr;
	systick_cvr_t	cvr;
	systick_calib_t calib;
} systick_t;
_Static_assert(sizeof(systick_t) == 4 * sizeof(size_t), "invalid systick_t size");
