#pragma once

#include "usb.h"

/**
 * @brief USB device vendor ID for CDC.
 */
#define USB_CDC_ID_VENDOR 0x0483

/**
 * @brief USB device vendor ID for CDC.
 */
#define USB_CDC_ID_PRODUCT 0x5740

/**
 * @brief USB device class used for CDC.
 */
#define USB_CDC_DEVICE_CLASS 0x2

/**
 * @brief USB device subclass used for CDC.
 */
#define USB_CDC_DEVICE_SUBCLASS 0x0

/**
 * @brief USB device protocol used for CDC.
 */
#define USB_CDC_DEVICE_PROTOCOL 0x0

/**
 * @brief USB interface class used for CDC management interface.
 */
#define USB_CDC_INTERFACE_MGMT_CLASS 0x2

/**
 * @brief USB interface subclass used for CDC management interface.
 */
#define USB_CDC_INTERFACE_MGMT_SUBCLASS 0x2

/**
 * @brief USB interface protocol used for CDC management interface.
 */
#define USB_CDC_INTERFACE_MGMT_PROTOCOL 0x1

/**
 * @brief USB interface class used for CDC data interface.
 */
#define USB_CDC_INTERFACE_DATA_CLASS 0xa

/**
 * @brief USB interface subclass used for CDC data interface.
 */
#define USB_CDC_INTERFACE_DATA_SUBCLASS 0x0

/**
 * @brief USB interface protocol used for CDC data interface.
 */
#define USB_CDC_INTERFACE_DATA_PROTOCOL 0x0

/**
 * @brief USB CDC descriptor type.
 */
typedef enum {
	USB_CDC_DESCRIPTOR_TYPE_CS_INTERFACE = 0x24,
	USB_CDC_DESCRIPTOR_TYPE_CS_ENDPOINT  = 0x25,
} usb_cdc_descriptor_type_t;

/**
 * @brief USB CDC descriptor subtype.
 */
typedef enum {
	USB_CDC_DESCRIPTOR_SUBTYPE_HEADER = 0x0,
	USB_CDC_DESCRIPTOR_SUBTYPE_ACM	  = 0x2,
	USB_CDC_DESCRIPTOR_SUBTYPE_UNION  = 0x6,
} usb_cdc_descriptor_subtype_t;

/**
 * @brief USB CDC standard revision.
 */
#define USB_CDC_REVISION 0x0110

/**
 * @brief USB CDC class request.
 */
typedef enum {
	USB_CDC_REQUEST_SEND_ENCAPSULATED_COMMAND = 0x00,
	USB_CDC_REQUEST_GET_ENCAPSULATED_RESPONSE = 0x01,
	USB_CDC_REQUEST_SET_LINE_CODING		  = 0x20,
	USB_CDC_REQUEST_GET_LINE_CODING		  = 0x21,
} usb_cdc_request_t;

/**
 * @brief USB CDC header descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t	 bFunctionLength;
	uint8_t	 bDescriptorType;
	uint8_t	 bDescriptorSubtype;
	uint16_t bcdCDC;
} usb_cdc_descriptor_header_t;
_Static_assert(sizeof(usb_cdc_descriptor_header_t) == sizeof(uint8_t) * 5, "invalid usb_cdc_descriptor_header_t size");

/**
 * @brief USB CDC interface union descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t bFunctionLength;
	uint8_t bDescriptorType;
	uint8_t bDescriptorSubtype;
	uint8_t bControlInterface;
	uint8_t bSubordinateInterface;
} usb_cdc_descriptor_union_t;
_Static_assert(sizeof(usb_cdc_descriptor_union_t) == sizeof(uint8_t) * 5, "invalid usb_cdc_descriptor_union_t size");

/**
 * @brief USB CDC ACM capabilities descriptor.
 */
typedef struct __attribute__((packed)) {
	uint8_t bFunctionLength;
	uint8_t bDescriptorType;
	uint8_t bDescriptorSubtype;
	union {
		uint8_t bmCapabilities;
		struct {
			bool commSupported		: 1;
			bool lineSupported		: 1;
			bool breakSupported		: 1;
			bool networkConnectionSupported : 1;
		};
	};
} usb_cdc_descriptor_acm_t;
_Static_assert(sizeof(usb_cdc_descriptor_acm_t) == sizeof(uint8_t) * 4, "invalid usb_cdc_descriptor_acm_t size");

/**
 * @brief USB CDC line coding.
 */
typedef enum {
	USB_CDC_LINE_CODING_CHAR_1   = 0,
	USB_CDC_LINE_CODING_CHAR_1_5 = 1,
	USB_CDC_LINE_CODING_CHAR_2   = 2,
} usb_cdc_line_coding_char_t;

/**
 * @brief USB CDC line parity.
 */
typedef enum {
	USB_CDC_LINE_CODING_PARITY_NONE	 = 0,
	USB_CDC_LINE_CODING_PARITY_ODD	 = 1,
	USB_CDC_LINE_CODING_PARITY_EVEN	 = 2,
	USB_CDC_LINE_CODING_PARITY_MARK	 = 3,
	USB_CDC_LINE_CODING_PARITY_SPACE = 4,
} usb_cdc_line_coding_parity_t;

/**
 * @brief USB CDC line number of bits.
 */
typedef enum {
	USB_CDC_LINE_CODING_DATA_BITS_5	 = 5,
	USB_CDC_LINE_CODING_DATA_BITS_6	 = 6,
	USB_CDC_LINE_CODING_DATA_BITS_7	 = 7,
	USB_CDC_LINE_CODING_DATA_BITS_8	 = 8,
	USB_CDC_LINE_CODING_DATA_BITS_16 = 16,
} usb_cdc_line_coding_data_bits_t;

/**
 * @brief USB CDC line configuration.
 */
typedef struct __attribute__((packed)) {
	uint32_t dwDTERate;
	uint8_t	 bCharFormat;
	uint8_t	 bParityType;
	uint8_t	 bDataBits;
} usb_cdc_line_coding_t;
_Static_assert(sizeof(usb_cdc_line_coding_t) == sizeof(uint8_t) * 7, "invalid usb_cdc_line_coding_t size");

/**
 * @brief USB CDC global state.
 */
typedef struct {
	usb_cdc_line_coding_t line_coding;	///< CDC line coding configuration.
	bool		      setting_line : 1; ///< CDC line setting is in progress.
} usb_cdc_global_state_t;

/**
 * @brief USB CDC global state instance.
 */
extern usb_cdc_global_state_t usb_cdc_global_state;

/**
 * @brief USB CDC endpoint setup callback.
 */
void usb_handle_cdc_setup(uint8_t ep);
