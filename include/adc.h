#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/adc.h"
#elif defined(set32f103h)
#include "../stm32f103h/adc.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @brief Configure temperature conversion.
 *
 * In current implementation ADC is used for temperature conversion exclusively.
 * It's not possible to use other channels to perform other tasks.
 *
 * @param[IN] int_enable Enable interrupt on the conversion end.
 * @param[IN] dma_enable Enable DMA transfer of converted data.
 */
void adc_temp_config(bool int_enable, bool dma_enable);

/**
 * @brief Calibrate ADC.
 *
 * @param[IN] base ADC to calibrate.
 */
void adc_calibrate(size_t base);

/**
 * @brief Start temperature conversion.
 */
void adc_temp_start(void);

/**
 * @brief Convert codes to temperature.
 * @param[IN] temp_code ADC temperature code.
 * @param[IN] ref_code ADC reference code.
 * @return Temperature in milli Celsius
 */
int32_t adc_temp_convert(int16_t temp_code, int16_t ref_code);
