#include <gpio.h>
#include <util.h>

void
gpio_configure_input(size_t bank, uint8_t pin_number, uint8_t mode, bool pull_level)
{
	gpio_t *gpio = GET_REG(bank);
	REG_T	cr   = pin_number >= 8 ? &(gpio->crh.val) : &(gpio->crl.val);
	size_t	val  = mode << 2;
	size_t	mask = 0xf;
	val <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	mask <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	// write all zeros before setting value
	REG_WRITE_MASK(cr, val, mask);
	if (mode == GPIO_IN_PULL) {
		REG_T odr = &(gpio->odr.val);
		REG_WRITE_MASK(odr, ((size_t) pull_level) << pin_number, 1 << pin_number);
	}
}

void
gpio_configure_output(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t speed)
{
	gpio_t *gpio = GET_REG(bank);
	REG_T	cr   = pin_number >= 8 ? &(gpio->crh.val) : &(gpio->crl.val);
	size_t	val  = mode << 2 | speed;
	size_t	mask = 0xf;
	val <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	mask <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	REG_WRITE_MASK(cr, val, mask);
}

void
gpio_lock(size_t bank, size_t pin_mask)
{
	gpio_t *gpio = GET_REG(bank);
	REG_T	lckr = &(gpio->lckr.val);
	while (!gpio->lckr.bits.lckk) {
		// lock sequence
		REG_WRITE(lckr, pin_mask | BIT(16));
		REG_WRITE(lckr, pin_mask & ~BIT(16));
		REG_WRITE(lckr, pin_mask | BIT(16));
		REG_VAL(lckr);
	}
}

size_t
gpio_read_pins(size_t bank, uint16_t pin_mask)
{
	gpio_t *gpio = GET_REG(bank);
	return gpio->idr.val & pin_mask;
}

bool
gpio_read_pin(size_t bank, uint8_t pin_number)
{
	return !!gpio_read_pins(bank, BIT(pin_number));
}

void
gpio_write_pins(size_t bank, uint16_t pin_mask, bool value)
{
	gpio_t *gpio = GET_REG(bank);
	REG_T	reg  = value ? &(gpio->bsrr.val) : &(gpio->brr.val);
	if (value) {
		REG_SET(reg, pin_mask);
	} else {
		REG_CLEAR(reg, pin_mask);
	}
}

void
gpio_write_pin(size_t bank, uint8_t pin_number, bool value)
{
	gpio_write_pins(bank, BIT(pin_number), value);
}

void
gpio_toggle_pins(size_t bank, uint16_t pin_mask)
{
	gpio_t *gpio = GET_REG(bank);
	REG_T	odr  = &(gpio->odr.val);
	REG_TOGGLE(odr, pin_mask);
}

void
gpio_toggle_pin(size_t bank, uint8_t pin_number)
{
	gpio_toggle_pins(bank, BIT(pin_number));
}
