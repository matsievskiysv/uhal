#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define GPIO_A 0x4001##0800##UL
#define GPIO_B 0x4001##0C00##UL
#define GPIO_C 0x4001##1000##UL
#define GPIO_D 0x4001##1400##UL
#define GPIO_E 0x4001##1800##UL
#define GPIO_F 0x4001##1C00##UL
#define GPIO_G 0x4001##2000##UL

typedef union {
	struct __attribute__((packed)) {
		uint8_t mode0 : 2;
		uint8_t cnf0  : 2;
		uint8_t mode1 : 2;
		uint8_t cnf1  : 2;
		uint8_t mode2 : 2;
		uint8_t cnf2  : 2;
		uint8_t mode3 : 2;
		uint8_t cnf3  : 2;
		uint8_t mode4 : 2;
		uint8_t cnf4  : 2;
		uint8_t mode5 : 2;
		uint8_t cnf5  : 2;
		uint8_t mode6 : 2;
		uint8_t cnf6  : 2;
		uint8_t mode7 : 2;
		uint8_t cnf7  : 2;
	} bits;
	size_t val;
} gpio_crl_t;
_Static_assert(sizeof(gpio_crl_t) == sizeof(size_t), "invalid gpio_crl_t size");

#define GPIO_IN_ANALOG 0b00
#define GPIO_IN_FLOAT  0b01
#define GPIO_IN_PULL   0b10
#define GPIO_OUT_PUSH  0b00
#define GPIO_OUT_OPEN  0b01
#define GPIO_ALT_PUSH  0b10
#define GPIO_ALT_OPEN  0b11

#define GPIO_SPEED_2MHZ	 0b10
#define GPIO_SPEED_10MHZ 0b01
#define GPIO_SPEED_50MHZ 0b11

typedef union {
	struct __attribute__((packed)) {
		uint8_t mode8  : 2;
		uint8_t cnf8   : 2;
		uint8_t mode9  : 2;
		uint8_t cnf9   : 2;
		uint8_t mode10 : 2;
		uint8_t cnf10  : 2;
		uint8_t mode11 : 2;
		uint8_t cnf11  : 2;
		uint8_t mode12 : 2;
		uint8_t cnf12  : 2;
		uint8_t mode13 : 2;
		uint8_t cnf13  : 2;
		uint8_t mode14 : 2;
		uint8_t cnf14  : 2;
		uint8_t mode15 : 2;
		uint8_t cnf15  : 2;
	} bits;
	size_t val;
} gpio_crh_t;
_Static_assert(sizeof(gpio_crh_t) == sizeof(size_t), "invalid gpio_crh_t size");

typedef union {
	struct __attribute__((packed)) {
		bool idr0  : 1;
		bool idr1  : 1;
		bool idr2  : 1;
		bool idr3  : 1;
		bool idr4  : 1;
		bool idr5  : 1;
		bool idr6  : 1;
		bool idr7  : 1;
		bool idr8  : 1;
		bool idr9  : 1;
		bool idr10 : 1;
		bool idr11 : 1;
		bool idr12 : 1;
		bool idr13 : 1;
		bool idr14 : 1;
		bool idr15 : 1;
	} bits;
	size_t val;
} gpio_idr_t;
_Static_assert(sizeof(gpio_idr_t) == sizeof(size_t), "invalid gpio_idr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool odr0  : 1;
		bool odr1  : 1;
		bool odr2  : 1;
		bool odr3  : 1;
		bool odr4  : 1;
		bool odr5  : 1;
		bool odr6  : 1;
		bool odr7  : 1;
		bool odr8  : 1;
		bool odr9  : 1;
		bool odr10 : 1;
		bool odr11 : 1;
		bool odr12 : 1;
		bool odr13 : 1;
		bool odr14 : 1;
		bool odr15 : 1;
	} bits;
	size_t val;
} gpio_odr_t;
_Static_assert(sizeof(gpio_odr_t) == sizeof(size_t), "invalid gpio_odr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool bs0  : 1;
		bool bs1  : 1;
		bool bs2  : 1;
		bool bs3  : 1;
		bool bs4  : 1;
		bool bs5  : 1;
		bool bs6  : 1;
		bool bs7  : 1;
		bool bs8  : 1;
		bool bs9  : 1;
		bool bs10 : 1;
		bool bs11 : 1;
		bool bs12 : 1;
		bool bs13 : 1;
		bool bs14 : 1;
		bool bs15 : 1;
		bool br0  : 1;
		bool br1  : 1;
		bool br2  : 1;
		bool br3  : 1;
		bool br4  : 1;
		bool br5  : 1;
		bool br6  : 1;
		bool br7  : 1;
		bool br8  : 1;
		bool br9  : 1;
		bool br10 : 1;
		bool br11 : 1;
		bool br12 : 1;
		bool br13 : 1;
		bool br14 : 1;
		bool br15 : 1;
	} bits;
	size_t val;
} gpio_bsrr_t;
_Static_assert(sizeof(gpio_bsrr_t) == sizeof(size_t), "invalid gpio_bsrr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool br0  : 1;
		bool br1  : 1;
		bool br2  : 1;
		bool br3  : 1;
		bool br4  : 1;
		bool br5  : 1;
		bool br6  : 1;
		bool br7  : 1;
		bool br8  : 1;
		bool br9  : 1;
		bool br10 : 1;
		bool br11 : 1;
		bool br12 : 1;
		bool br13 : 1;
		bool br14 : 1;
		bool br15 : 1;
	} bits;
	size_t val;
} gpio_brr_t;
_Static_assert(sizeof(gpio_brr_t) == sizeof(size_t), "invalid gpio_brr_t size");

typedef union {
	struct __attribute__((packed)) {
		bool lck0  : 1;
		bool lck1  : 1;
		bool lck2  : 1;
		bool lck3  : 1;
		bool lck4  : 1;
		bool lck5  : 1;
		bool lck6  : 1;
		bool lck7  : 1;
		bool lck8  : 1;
		bool lck9  : 1;
		bool lck10 : 1;
		bool lck11 : 1;
		bool lck12 : 1;
		bool lck13 : 1;
		bool lck14 : 1;
		bool lck15 : 1;
		bool lckk  : 1;
	} bits;
	size_t val;
} gpio_lckr_t;
_Static_assert(sizeof(gpio_lckr_t) == sizeof(size_t), "invalid gpio_lckr_t size");

typedef volatile struct {
	gpio_crl_t  crl;
	gpio_crh_t  crh;
	gpio_idr_t  idr;
	gpio_odr_t  odr;
	gpio_bsrr_t bsrr;
	gpio_brr_t  brr;
	gpio_lckr_t lckr;
} gpio_t;
_Static_assert(sizeof(gpio_t) == 7 * sizeof(size_t), "invalid gpio_t size");
